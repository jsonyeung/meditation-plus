import { LOGOUT_DONE } from '../actions/auth.actions';

export function clearState(reducer) {
  return function (state, action) {

    if (action.type === LOGOUT_DONE) {
      state = undefined;
    }

    return reducer(state, action);
  };
}
