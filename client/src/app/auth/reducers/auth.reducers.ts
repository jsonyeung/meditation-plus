import * as auth from '../actions/auth.actions';
import { AppState } from 'app/reducers';
import { createSelector } from '@ngrx/store';
import { AuthService } from '../../shared/auth.service';
import { Profile } from 'app/profile/profile';
import * as moment from 'moment';

export interface AuthState {
  loggedIn: boolean;
  tokenExpires: number;
  profile: Profile | null;
  loading: boolean;
  error: string;
}

export const initialAuthState: AuthState = {
  loggedIn: false,
  profile: null,
  tokenExpires: 0,
  loading: false,
  error: ''
};

export function authReducer(
  state = initialAuthState,
  action: auth.Actions
): AuthState {
  switch (action.type) {
    case auth.LOGIN: {
      return { ...state, loading: true, error: '' };
    }
    case auth.LOGIN_ERROR: {
      return { ...state, loading: false, error: action.payload };
    }
    case auth.LOGIN_DONE: {
      const profile = {
        ...action.payload.profile,
        lastLike: moment(action.payload.profile.lastLike)
      };

      return {
        ...state,
        error: '',
        loading: false,
        loggedIn: true,
        tokenExpires: action.payload.tokenExpires,
        profile
      };
    }
    case auth.RELOAD_PROFILE_DONE: {
      const profile = {
        ...action.payload,
        lastLike: moment(action.payload.lastLike)
      };
      return {
        ...state,
        profile
      };
    }
    case auth.LOGOUT: {
      return { ...state, loading: true, error: '' };
    }
    case auth.LOGOUT_DONE: {
      return { ...initialAuthState };
    }
    case auth.LOGOUT_ERROR: {
      return { ...state, loading: false, error: action.payload };
    }
    case auth.SIGNUP: {
      return { ...state, loading: true, error: '' };
    }
    case auth.SIGNUP_DONE: {
      return { ...state, loading: false, error: '' };
    }
    case auth.SIGNUP_ERROR: {
      return { ...state, loading: false, error: action.payload };
    }

    default: {
      return state;
    }
  }
}

// Selectors for easy access
export const selectAuth = (state: AppState) => state.auth;

export const selectIsLoggedIn = createSelector(selectAuth, (state: AuthState) => state.loggedIn);
export const selectId = createSelector(selectAuth,
  (state: AuthState) => state.profile ? state.profile._id : null
);
export const selectRole = createSelector(selectAuth,
  (state: AuthState) => state.profile ? state.profile.role : null
);
export const selectTokenExpires = createSelector(selectAuth, (state: AuthState) => state.tokenExpires);
export const selectLoading = createSelector(selectAuth, (state: AuthState) => state.loading);
export const selectError = createSelector(selectAuth, (state: AuthState) => state.error);
export const selectProfile = createSelector(selectAuth, (state: AuthState) => state.profile);
export const selectAdmin = createSelector(selectAuth,
  (state: AuthState) => state.profile && state.profile.role
    && (state.profile.role === AuthService.adminRole
    || state.profile.role === AuthService.teacherAdminRole)
);
export const selectTeacher = createSelector(selectAuth,
  (state: AuthState) => state.profile && state.profile.role &&
    (state.profile.role === AuthService.teacherAdminRole || state.profile.role === AuthService.teacherRole)
);
export const selectTokenExpired = createSelector(selectTokenExpires, exp => {
  const date = new Date(0);
  date.setUTCSeconds(exp);
  if (date === null) {
    return true;
  }

  return !(date.valueOf() > new Date().valueOf());
});
