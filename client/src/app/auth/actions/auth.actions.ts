import { Action } from '@ngrx/store';
import { Profile } from '../../profile/profile';

export const LOGIN = '[Auth] Login';
export const LOGIN_DONE = '[Auth] Login Done';
export const LOGIN_ERROR = '[Auth] Login Error';
export const LOGOUT = '[Auth] Logout';
export const LOGOUT_DONE = '[Auth] Logout Done';
export const LOGOUT_ERROR = '[Auth] Logout Error';
export const SIGNUP = '[Auth] Signup';
export const SIGNUP_DONE = '[Auth] Signup Done';
export const SIGNUP_ERROR = '[Auth] Signup Error';
export const RELOAD_PROFILE = '[Auth] Reload Profile';
export const RELOAD_PROFILE_DONE = '[Auth] Reload Profile Done';

export interface LoginPayload {
  email: string;
  username?: string;
  password: string;
  acceptGdpr?: boolean;
  deleteAccount?: boolean;
}

export class Login implements Action {
  readonly type = LOGIN;
  constructor(public payload: LoginPayload) {}
}

export interface LoginDonePayload {
  tokenExpires: number;
  profile: Profile;
}

export class LoginDone implements Action {
  readonly type = LOGIN_DONE;
  constructor(public payload: LoginDonePayload) {}
}

export class LoginError implements Action {
  readonly type = LOGIN_ERROR;
  constructor(public payload: string) {}
}

export class ReloadProfile implements Action {
  readonly type = RELOAD_PROFILE;
}

export class ReloadProfileDone implements Action {
  readonly type = RELOAD_PROFILE_DONE;
  constructor(public payload: Profile) {}
}

export class Logout implements Action {
  readonly type = LOGOUT;
  constructor() {}
}

export class LogoutDone implements Action {
  readonly type = LOGOUT_DONE;
  constructor() {}
}

export class LogoutError implements Action {
  readonly type = LOGOUT_ERROR;
  constructor(public payload: string) {}
}

export interface SignupPayload {
  username: string;
  password: string;
  email: string;
  name: string;
  acceptGdpr: boolean;
}

export class Signup implements Action {
  readonly type = SIGNUP;
  constructor(public payload: SignupPayload) {}
}

export class SignupDone implements Action {
  readonly type = SIGNUP_DONE;
  constructor(public payload: string) {}
}

export class SignupError implements Action {
  readonly type = SIGNUP_ERROR;
  constructor(public payload: string) {}
}

export type Actions = Login
 | LoginDone
 | LoginError
 | Logout
 | LogoutDone
 | LogoutError
 | Signup
 | SignupDone
 | SignupError
 | ReloadProfile
 | ReloadProfileDone
;
