import { Component } from '@angular/core';
import { CommitmentService } from './commitment.service';
import { UserService } from '../user/user.service';
import { Store } from '@ngrx/store';
import { AppState } from '../reducers';
import { SetTitle } from '../actions/global.actions';
import { selectId } from '../auth/reducers/auth.reducers';
import { concatMap, take, map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'commitment',
  templateUrl: './commitment.component.html',
  styleUrls: [
    './commitment.component.styl'
  ]
})
export class CommitmentComponent {

  // commitment data
  commitments: any;
  subscribedCommitments: Object = {};
  profile;
  loadedInitially = false;

  userId$: Observable<string>;
  show = {};

  constructor(
    private commitmentService: CommitmentService,
    private userService: UserService,
    store: Store<AppState>
  ) {
    store.dispatch(new SetTitle('Commitments'));
    this.userId$ = store.select(selectId);

    this.loadCommitments();

    // load own profile to calculate achievements
    this.userId$.pipe(
      take(1),
      concatMap(id => this.userService.getProfile(id))
    )
    .subscribe(
      res => this.profile = res,
      err => console.error(err)
    );
  }

  /**
   * Loads all commitments
   */
  loadCommitments() {
    this.commitmentService
      .getAll()
      .subscribe(res => {
        this.commitments = res;
        this.loadedInitially = true;
        this.loadSubscribedCommitments();
      });
  }

  /**
   * Loads subscribed commitments with progress
   */
  loadSubscribedCommitments() {
    this.commitmentService
      .getCurrentUser()
      .subscribe(res => this.subscribedCommitments = res);
  }

  /**
   * Commits to Commitment
   */
  commit(evt, commitment) {
    evt.preventDefault();

    this.commitmentService.commit(commitment)
      .subscribe(
        () => this.loadCommitments(),
        err => console.error(err)
      );
  }

  /**
   * Uncommits from Commitment
   */
  uncommit(evt, commitment) {
    evt.preventDefault();

    this.commitmentService.uncommit(commitment)
      .subscribe(
        () => this.loadCommitments(),
        err => console.error(err)
      );
  }

  /**
   * Checks whether user is committed to commitment.
   */
  committed(commitment): Observable<boolean> {
    if (!commitment.users) {
      return of(false);
    }

    return this.userId$.pipe(
      take(1),
      map(userId => {
        for (const { user } of commitment.users) {
          if (user && user._id === userId) {
            return true;
          }
        }
        return false;
      })
    );
  }

  showAll(commitment): boolean {
    return this.show.hasOwnProperty(commitment._id);
  }

  doShow(commitment) {
    this.show[commitment._id] = true;
  }
}
