import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionComponent } from './question.component';
import { UserService } from '../user/user.service';
import { FakeUserService } from '../user/testing/fake-user.service';
import { FormsModule } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { QuestionService } from './question.service';
import { FakeQuestionService } from './testing/fake-question.service';
import { MockComponent } from 'ng2-mock-component';
import { MaterialModule } from '../shared/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { UserTextListModule } from 'app/user-text-list/user-text-list.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SharedModule } from '../shared';
import { SettingsService } from '../shared';
import { FakeSettingsService } from '../shared/testing/fake-settings.service';
import { RouterTestingModule } from '@angular/router/testing';
import { Store } from '@ngrx/store';
import { MockStore } from 'testing/mock.store';

describe('QuestionComponent', () => {
  let component: QuestionComponent;
  let fixture: ComponentFixture<QuestionComponent>;
  const store = new MockStore();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        NoopAnimationsModule,
        UserTextListModule,
        SharedModule,
        RouterTestingModule,
        HttpClientTestingModule
      ],
      declarations: [
        QuestionComponent,
        MockComponent({selector: 'emoji-select'}),
        MockComponent({selector: 'question-suggestions', inputs: ['currentSearch']}),
        MockComponent({selector: 'question-list-entry', inputs: ['question']}),

      ],
      providers: [
        {provide: QuestionService, useClass: FakeQuestionService},
        {provide: UserService, useClass: FakeUserService},
        {provide: SettingsService, useClass: FakeSettingsService},
        {provide: Store, useValue: store}
      ]

    })
      .compileComponents();
  }));

  beforeEach(() => {
    store.defaultValue = [];
    fixture = TestBed.createComponent(QuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

});
