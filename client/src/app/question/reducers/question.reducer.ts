import * as _ from 'lodash';
import * as question from '../actions/question.actions';
import { AppState } from '../../reducers';
import { createSelector } from '@ngrx/store';
import { Question } from '../question';

export interface QuestionState {
  unanswered: Question[];
  answered: Question[];
  answeredPage: number;
  noMorePages: boolean;
  loadingAnswered: 'search' | 'more' | false;
  loadingUnanswered: boolean;
  loadedInitially: boolean;
  posting: boolean;
  lastFilter: question.LoadAnsweredQuestionsQuery;
  currentIframeQuestionId: string;
}

export const initialQuestionState: QuestionState = {
  unanswered: [],
  answered: [],
  answeredPage: 0,
  noMorePages: false,
  loadingAnswered: false,
  loadingUnanswered: false,
  loadedInitially: false,
  posting: false,
  lastFilter: question.loadAnsweredQuestionInitialQuery,
  currentIframeQuestionId: ''
};

const questionsPerPage = 10;

export function questionReducer(
  state = initialQuestionState,
  action: question.Actions
): QuestionState {
  switch (action.type) {
    case question.LOAD_UNANSWERED: {
      return { ..._.cloneDeep(state), loadingUnanswered: true };
    }
    case question.LOAD_UNANSWERED_DONE: {
      return {
        ..._.cloneDeep(state),
        loadingUnanswered: false,
        loadedInitially: true,
        unanswered: action.payload
      };
    }
    case question.LOAD_ANSWERED: {
      return {
        ..._.cloneDeep(state),
        loadingAnswered: action.payload.intention,
        answered: action.payload.intention === 'search' ? [] : state.answered,
        answeredPage: action.payload.intention === 'more'
          ? state.answeredPage + 1
          : 0,
        lastFilter: action.payload.query,
        currentIframeQuestionId: ''
      };
    }
    case question.LOAD_ANSWERED_DONE: {
      return {
        ..._.cloneDeep(state),
        loadingAnswered: false,
        answered: action.payload.intention === 'more'
          ? [...state.answered, ...action.payload.questions]
          : action.payload.questions,
        noMorePages: action.payload.questions.length < questionsPerPage
      };
    }
    case question.POST: {
      return { ..._.cloneDeep(state), posting: true };
    }
    case question.POST_DONE: {
      return { ..._.cloneDeep(state), posting: false };
    }

    case question.TOGGLE_YOUTUBE_ANSWER: {
      return { ..._.cloneDeep(state), currentIframeQuestionId: action.payload };
    }

    default: {
      return state;
    }
  }
}

// Selectors for easy access
export const selectQuestions = (state: AppState) => state.questions;
export const selectUnanswered = createSelector(selectQuestions, (state: QuestionState) => state.unanswered);
export const selectAnswered = createSelector(selectQuestions, (state: QuestionState) => state.answered);
export const selectAnsweredPage = createSelector(selectQuestions, (state: QuestionState) => state.answeredPage);
export const selectNoMorePages = createSelector(selectQuestions, (state: QuestionState) => state.noMorePages);
export const selectLoadingAnswered = createSelector(selectQuestions, (state: QuestionState) => state.loadingAnswered);
export const selectLoadingUnanswered = createSelector(selectQuestions, (state: QuestionState) => state.loadingUnanswered);
export const selectLoadedInitially = createSelector(selectQuestions, (state: QuestionState) => state.loadedInitially);
export const selectPosting = createSelector(selectQuestions, (state: QuestionState) => state.posting);
export const selectLastFilter = createSelector(selectQuestions, (state: QuestionState) => state.lastFilter);
export const selectCurrentYoutubeAnswer = createSelector(selectQuestions, (state: QuestionState) => state.currentIframeQuestionId);

