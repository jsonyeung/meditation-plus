import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { InputDialogComponent } from '../../dialog/input-dialog/input-dialog.component';
import { map, filter } from 'rxjs/operators';
import { HasTimecodePipe } from './has-timecode.pipe';
import { Question } from '../question';

@Component({
  selector: 'app-suggest-videourl-dialog',
  templateUrl: './suggest-videourl-dialog.component.html',
  styleUrls: ['./suggest-videourl-dialog.component.styl']
})
export class SuggestVideoUrlDialogComponent {

  url = '';
  group: FormGroup;
  manualUrl: FormControl;
  time: FormControl;

  constructor(
    public dialogRef: MatDialogRef<InputDialogComponent>,
    builder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: { question?: Question }
  ) {
    const hasUrl = data && data.question && data.question.broadcast && data.question.broadcast.videoUrl;
    this.manualUrl = builder.control({
      value: hasUrl
        ? data.question.broadcast.videoUrl
        : '',
      disabled: hasUrl
    });
    this.time = builder.control('');
    this.group = builder.group({
      url: this.manualUrl,
      time: this.time
    });

    // reset url on empty inputs
    this.group.valueChanges.pipe(
      filter(val => !this.group.valid || val.url === '' || val.time === ''),
    ).subscribe(() => this.url = '');

    const hasTimecode = new HasTimecodePipe();

    // use url with time information
    this.group.valueChanges.pipe(
      filter(() => this.group.valid),
      filter(val => val.url !== '' && val.time === '' && hasTimecode.transform(val.url)),
      map(group => {
        const val: string = group.url;

        const query = val.includes('?')
          ? this.queryStringToObject(val.split('?')[1])
          : {};

        if (val.includes('youtu.be')) {
          const queryStart = val.indexOf('.be/') + 4;
          let queryEnd = val.indexOf('?t=');
          queryEnd = queryEnd >= 0 ? queryEnd : val.length;
          query.v = val.substr(queryStart, queryEnd - queryStart);
        }

        return `https://youtu.be/${query.v}?t=${query.t}`;
      }),
    ).subscribe(val => this.url = val);

    // create url from manual time input
    this.group.valueChanges.pipe(
      filter(() => this.group.valid),
      filter(val => (val.url !== '' || hasUrl) && val.time !== ''),
      map(val => {
        const result = val.time.split(':').map(num => parseInt(num, 10));
        let time = 0;
        if (result.length === 1) {
          time = result[0];
        } else if (result.length === 2) {
          time = result[0] * 60 + result[1];
        } else {
          time = result[0] * 3600 + result[1] * 60 + result[2];
        }
        return { ...val, time };
      }),
      map(val => {
        const url = hasUrl ? data.question.broadcast.videoUrl : val.url;

        if (url.includes('youtu.be')) {
          return `${url}?t=${val.time}`;
        }

        const query = this.queryStringToObject(url.split('?')[1]);
        return `https://youtu.be/${query.v}?t=${val.time}`;
      }),
    ).subscribe(val => this.url = val);
  }

  queryStringToObject(queryString: string): any {
    return queryString
      .replace(/(^\?)/, '')
      .split('&')
      .map(function(n) { return n = n.split('='), this[n[0]] = n[1], this; }.bind({}))[0];
  }

  open() {
    window.open(this.url);
  }

  submit() {
    this.dialogRef.close(this.url);
  }

}
