import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiConfig } from '../../api.config';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Feedback } from './feedback';

@Injectable({
  providedIn: 'root',
})
export class FeedbackService {
  constructor(private http: HttpClient) {}

  getFeedback(page: number, perPage: number = 10): Observable<Feedback[]> {
    const params = new HttpParams({
      fromObject: {
        page: page.toString(),
        perPage: perPage.toString(),
      },
    });

    return this.http.get<Feedback[]>(`${ApiConfig.url}/api/feedback`, {
      params,
    });
  }

  postFeedback(feedback: Feedback): Observable<Feedback> {
    return this.http.post<Feedback>(`${ApiConfig.url}/api/feedback`, feedback);
  }

  deleteFeedback(id: string): Observable<any> {
    return this.http.delete(`${ApiConfig.url}/api/feedback/${id}`);
  }
}
