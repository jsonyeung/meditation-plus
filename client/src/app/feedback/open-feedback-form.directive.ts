import { Directive, ElementRef, OnInit } from '@angular/core';
import { FeedbackFormComponent } from './feedback-form/feedback-form.component';
import { MatDialog } from '@angular/material';

@Directive({
  selector: '[feedbackForm]'
})
export class FeedbackFormDirective implements OnInit {

  constructor(private elementRef: ElementRef, private dialog: MatDialog) { }

  ngOnInit() {
    this.elementRef.nativeElement.onclick = () => {
      this.dialog.open(FeedbackFormComponent, {
        panelClass: 'feedback-form-modal'
      });
    };
  }
}
