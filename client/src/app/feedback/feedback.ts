import { DeviceType } from './device-type';

export interface Feedback {
  text: string;
  anonymous?: boolean;
  rating?: number;
  device?: DeviceType;
  __v?: number;
  _id?: string;
  updatedAt?: string;
  createdAt?: string;
  user?: {
    _id: string;
    _v: number;
    username: string;
    name: string;
  };
}
