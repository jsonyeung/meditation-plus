import { Injectable } from '@angular/core';
import { ApiConfig } from '../../api.config';
import { WebsocketService } from '../shared';
import { Message } from './message';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { concatMap } from 'rxjs/operators';

@Injectable()
export class MessageService {

  public constructor(
    private http: HttpClient,
    private wsService: WebsocketService
  ) {
  }

  public getRecent(page = 0): Observable<any> {
    let params = new HttpParams();
    params = params.append('page', '' + page);

    return this.http.get(ApiConfig.url + '/api/message', { params });
  }

  public post(message: string): Observable<any> {
    return this.http.post(
      ApiConfig.url + '/api/message',
      JSON.stringify({ text: message })
    );
  }

  public update(message: Message): Observable<any> {
    return this.http.put(
      ApiConfig.url + '/api/message/' + message._id,
      JSON.stringify({ text: message.text })
    );
  }

  public delete(message: Message): Observable<any> {
    return this.http.delete(
      ApiConfig.url + '/api/message/' + message._id
    );
  }

  public synchronize(timeFrameStart: moment.Moment, timeFrameEnd: moment.Moment, countOnly: Boolean = false): Observable<any> {
    return this.http.post(
      ApiConfig.url + '/api/message/synchronize',
      JSON.stringify({ timeFrameStart, timeFrameEnd, countOnly})
    );
  }

  public getUpdateSocket(): Observable<any> {
    return this.wsService.getSocket().pipe(
      concatMap(websocket => Observable.create(obs => {
        websocket.on('message-update', res => obs.next(res));
      }))
    );
  }

  /**
   * Save the datetime last received message to local storage
   */
  public setLastMessage(messageDate) {
    window.localStorage.setItem('lastMessage', messageDate);
  }

  /**
   * Get the datetime last received message from local storage
   */
  public getLastMessage(): string {
    return window.localStorage.getItem('lastMessage');
  }
}
