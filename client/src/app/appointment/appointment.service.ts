import { Injectable } from '@angular/core';
import { ApiConfig } from '../../api.config';
import { WebsocketService } from '../shared';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { concatMap } from 'rxjs/operators';
import { Appointment } from './appointment';

@Injectable()
export class AppointmentService {

  public constructor(
    private http: HttpClient,
    private wsService: WebsocketService
  ) {
  }

  public getOwnAppointmentStatus(): Observable<any> {
    return this.http.get(ApiConfig.url + '/api/appointment/meeting');
  }

  public getAll(): Observable<any> {
    return this.http.get(ApiConfig.url + '/api/appointment');
  }

  public get(id: string): Observable<any> {
    return this.http.get(ApiConfig.url + '/api/appointment/' + id);
  }

  public listTeachers(): Observable<any> {
    return this.http.get(ApiConfig.url + '/api/appointment/teachers');
  }

  public getAggregated(teacherId: string): Observable<any> {
    return this.http.get(ApiConfig.url + '/api/appointment/aggregated/' + teacherId);
  }

  public create(teacherId: string, appointment: object): Observable<any> {
    return this.http.post(
      ApiConfig.url + '/api/appointment/' + teacherId,
      appointment,
      { observe: 'response', responseType: 'text' }
    );
  }

  public save(appointment): Observable<any> {
    const method = appointment._id ? 'put' : 'post';

    return this.http[method](
      ApiConfig.url + '/api/appointment' + (appointment._id ? '/' + appointment._id : ''),
      appointment,
      { observe: 'response', responseType: 'text' }
    );
  }

  public registration(appointment): Observable<any> {
    return this.http.post(
      `${ApiConfig.url}/api/appointment/${appointment._id}/register`,
      ''
    );
  }

  /**
   * Initializes Socket.io client with Jwt and listens to 'appointment'.
   */
  public getSocket(): Observable<any> {
    return this.wsService.getSocket().pipe(
      concatMap(websocket => Observable.create(obs => {
        websocket.on('appointment', res => obs.next(res));
      }))
    );
  }

  public delete(appointment): Observable<any> {
    return this.http.delete(ApiConfig.url + '/api/appointment/' + appointment._id);
  }

  public deleteRegistration(app: Appointment): Observable<any> {
    return this.http.delete(
      `${ApiConfig.url}/api/appointment/remove/${app.teacher}/${app._id}`
    );
  }

  public toggle(teacherId: string, hour: number, day: number): Observable<any> {
    return this.http.post(
      ApiConfig.url + '/api/appointment/toggle/' + teacherId,
      { hour, day },
      { observe: 'response', responseType: 'text' }
    );
  }

  public update(teacherId: string, oldHour: number, newHour: number): Observable<any> {
    return this.http.post(
      ApiConfig.url + '/api/appointment/update/' + teacherId,
      { oldHour, newHour }
    );
  }
}
