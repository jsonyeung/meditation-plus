import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppointmentScheduleComponent } from './appointment-schedule.component';
import { MaterialModule } from '../../shared/material.module';
import { RouterTestingModule } from '@angular/router/testing';
import { MomentModule } from 'ngx-moment';
import { SharedModule } from '../../shared';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Store } from '@ngrx/store';
import { MockStore } from 'testing/mock.store';
import { MockEffect } from 'testing/mock.effect';
import { Actions } from '@ngrx/effects';
import { MockActions } from 'testing/mock.actions';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { FakeAppointmentService } from '../testing/fake-appointment.service';
import { AppointmentService } from '..';
import { UserService } from '../../user/user.service';
import { FakeUserService } from '../../user/testing/fake-user.service';

describe('AppointmentScheduleComponent', () => {
  let component: AppointmentScheduleComponent;
  let fixture: ComponentFixture<AppointmentScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MaterialModule,
        RouterTestingModule,
        MomentModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        NoopAnimationsModule
      ],
      providers: [
        {provide: Store, useClass: MockStore},
        {provide: AppointmentService, useClass: FakeAppointmentService},
        {provide: AppointmentScheduleComponent, useClass: MockEffect},
        {provide: UserService, useClass: FakeUserService},
        {provide: Actions, useClass: MockActions}
      ],
      declarations: [ AppointmentScheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppointmentScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
