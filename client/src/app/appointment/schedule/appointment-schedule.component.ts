import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { AppointmentUtils } from '../appointment.utils';
import * as moment from 'moment-timezone';
import { filter, take, concatMap, withLatestFrom, startWith } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { SetTitle, ThrowError } from '../../actions/global.actions';
import { AppState } from '../../reducers';
import { interval, Observable, of, Subscription } from 'rxjs';
import { selectId, selectTeacher } from '../../auth/reducers/auth.reducers';
import { DialogService } from '../../dialog/dialog.service';
import { MatSelectChange, MatSnackBar } from '@angular/material';
import {
  CheckOwnAppointment,
  LoadAppointments,
  SelectTeacher,
  SetHourFormat,
  SetTimezone
} from '../actions/appointment.actions';
import {
  selectAllTeachers,
  selectAppointsByLocalDay,
  selectAppointsByLocalHour,
  selectAppointsLocalHours,
  selectCurrentTeacher,
  selectLoadingAppointments,
  selectLocalTimezone,
  selectOwnAppointment,
  selectOwnAppointmentStatus,
  selectWarnings
} from '../reducers/appointment.reducers';
import { Appointment, OwnAppointmentStatus } from '../appointment';
import { AppointmentService } from '../appointment.service';
import { UserService } from 'app/user/user.service';

@Component({
  selector: 'appointment-schedule',
  templateUrl: './appointment-schedule.component.html',
  styleUrls: [
    './appointment-schedule.component.styl'
  ]
})
export class AppointmentScheduleComponent implements OnInit, OnDestroy {
  ownAppointmentCountdown = '';
  currentTableView = 'table';
  hideFreeSlots = false;
  checkMeetingInterval: Subscription;

  appointmentsLocalHours$: Observable<string[]>;
  appointmentsByLocalHour$: Observable<{ [key: string]: Appointment[] }>;
  appointmentsByLocalDay$: Observable<{ [key: string]: Appointment[] }>;
  allTeachers$: Observable<object[]>;
  selectedTeacher$: Observable<any>;
  ownAppointment$: Observable<Appointment>;
  ownAppointmentStatus$: Observable<OwnAppointmentStatus>;
  warnings$: Observable<string[]>;
  localTimezone$: Observable<string>;
  loadingInitially$: Observable<boolean>;

  userId$: Observable<string>;
  teacher$: Observable<boolean>;

  weekdays = moment.weekdays();

  profile;

  constructor(
    private appointmentService: AppointmentService,
    private dialog: DialogService,
    private snackbar: MatSnackBar,
    private store: Store<AppState>,
    private sanitizer: DomSanitizer,
    private userService: UserService
  ) {
    store.dispatch(new SetTitle('At-Home Meditation Course'));

    this.appointmentsLocalHours$ = store.select(selectAppointsLocalHours);
    this.appointmentsByLocalHour$ = store.select(selectAppointsByLocalHour);
    this.appointmentsByLocalDay$ = store.select(selectAppointsByLocalDay);

    this.allTeachers$ = store.select(selectAllTeachers);
    this.selectedTeacher$ = store.select(selectCurrentTeacher);
    this.ownAppointment$ = store.select(selectOwnAppointment);
    this.ownAppointmentStatus$ = store.select(selectOwnAppointmentStatus);
    this.warnings$ = store.select(selectWarnings);

    this.localTimezone$ = store.select(selectLocalTimezone);
    this.userId$ = store.select(selectId);
    this.teacher$ = store.select(selectTeacher);
    this.loadingInitially$ = store.select(selectLoadingAppointments);

    this.checkMeetingInterval = interval(3000)
      .pipe(
        startWith(0),
        withLatestFrom(store.select(selectOwnAppointment)),
        filter(([i, ownAppoint]) => !!ownAppoint)
      )
      .subscribe(([i, ownAppoint]) => {
        this.ownAppointmentCountdown = AppointmentUtils
          .getCountdownForOwnAppointment(ownAppoint);

        store.dispatch(new CheckOwnAppointment());
      });
  }

  selectTeacher(e: MatSelectChange) {
    this.store.dispatch(new SelectTeacher(e.value));
  }

  getUtcHour(app: Appointment) {
    return moment(app.nextDate).utc().format('HH:mm z');
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.currentTableView = event.target.innerWidth > 800 ? 'table' : 'list';
  }

  sanitize(url: string) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }

  /**
   * Book or unbook own registration
   */
  toggleRegistration(evt, appointment) {
    evt.preventDefault();

    this.userId$.pipe(
      take(1),
      filter(id => !appointment.user || appointment.user._id === id),
      concatMap(() => !appointment.user ? of(true) : this.dialog.confirmDelete()),
      filter(val => !!val),
      concatMap(() => this.appointmentService.registration(appointment))
    )
    .subscribe(() => {
      this.snackbar.open('Your registration has been toggled.');
      this.ownAppointmentCountdown = '';
      this.store.dispatch(new LoadAppointments());
    }, error => {
      if (error.status === 400) {
        this.snackbar.open(error.error);
      } else {
        this.store.dispatch(new ThrowError({ error }));
      }
    });
  }

  /**
   * Remove appointment as teacher
   */
  removeRegistration(evt, appointment) {
    evt.preventDefault();

    this.teacher$.pipe(
      take(1),
      filter(val => !!val),
      concatMap(() => this.dialog.confirmDelete()),
      filter(val => !!val),
      concatMap(() => this.appointmentService.deleteRegistration(appointment))
    ).subscribe(
      () => {
        this.snackbar.open('The registration has been removed.');
        this.store.dispatch(new LoadAppointments());
      },
      error => this.store.dispatch(new ThrowError({ error }))
    );
  }

  ngOnInit() {
    this.currentTableView = window.innerWidth > 800 ? 'table' : 'list';

    this.userId$.pipe(
      take(1),
      concatMap(id => this.userService.getProfile(id))
    )
    .subscribe(
      res => {
        if (moment.tz.zone(res.timezone)) {
          this.store.dispatch(new SetTimezone(res.timezone));
        }

        this.profile = res;

        const profileTimeFormat = res.prefer12HourFormat
          ? 'h:mm A'
          : 'HH:mm';

        this.store.dispatch(new SetHourFormat(profileTimeFormat));
        this.store.dispatch(new LoadAppointments());
      },
      err => {
        console.log(err);
        this.store.dispatch(new LoadAppointments());
      }
    );
  }

  ngOnDestroy() {
    if (this.checkMeetingInterval) {
      this.checkMeetingInterval.unsubscribe();
    }
  }
}
