export interface Appointment {
  _id: string;
  teacher: string | object;
  user?: any;
  hour: number;
  weekDay: number;
  createdAt?: Date;
  updatedAt?: Date;
  nextDate?: Date;
}

export interface OwnAppointmentStatus {
  isActive: boolean;
  callInformation: string;
}

export interface AppointmentHourVO {
  status?: null | 'editing' | 'loading' | 'editLoading' | 'success' | 'error';
  errorMessage?: string;
  days?: any;
}
