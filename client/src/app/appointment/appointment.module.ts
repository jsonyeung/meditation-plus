import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { MomentModule } from 'ngx-moment';
import { SharedModule } from '../shared';
import { ProfileModule } from '../profile';
import { AppointmentComponent } from './appointment.component';
import { AppointmentScheduleComponent } from './schedule/appointment-schedule.component';
import { EffectsModule } from '@ngrx/effects';

import { LoadAppointmentsEffect } from './effects/load-appointments.effect';
import { CheckOwnAppointmentEffect } from './effects/check-own-appointment.effect';
import { ChangeTimeEffect } from './effects/change-time-format.effect';
const routes: Routes = [
  { path: '', component: AppointmentComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    SharedModule,
    ProfileModule,
    FormsModule,
    ReactiveFormsModule,
    MomentModule,
    EffectsModule.forFeature([
      LoadAppointmentsEffect,
      CheckOwnAppointmentEffect,
      ChangeTimeEffect
    ]),
  ],
  declarations: [
    AppointmentComponent,
    AppointmentScheduleComponent
  ],
  exports: [
    AppointmentComponent,
    AppointmentScheduleComponent
  ]
})
export class AppointmentModule { }
