import { Appointment } from './appointment';
import * as moment from 'moment-timezone';
import { sortBy, get as gv, find } from 'lodash';
import { Profile } from 'app/profile/profile';

const NUM_MAX_WEEKS_TIMEZONE_WARNING = 6;

export class AppointmentUtils {

  static DEFAULT_TEACHER_USERNAME = 'yuttadhammo';

  static findWarningsForFutureAppointments(
    ownAppointment: Appointment,
    allTeachers: any[],
    userTimezone: string,
    profileTimeFormat: string
  ): string[] {
    if (!ownAppointment) {
      return [];
    }

    const teacher = find(allTeachers, ({ _id }) => _id === ownAppointment.teacher);
    if (!teacher) {
      return [];
    }

    const warnings = [];
    const nextDate = moment.tz(ownAppointment.nextDate, teacher.timezone);

    // check the timezone conversion from the original timezone (i.e. EDT)
    // to the user's timezone for the next few appointments and
    // display a warning if the local time changes for one.
    for (let i = -1; i < NUM_MAX_WEEKS_TIMEZONE_WARNING; i++) {
      const regularDate = nextDate.clone().add(i * 7, 'days').tz(userTimezone);
      const regularTime = regularDate.format(profileTimeFormat);

      const futureDate = nextDate.clone().add((i + 1) * 7, 'days').tz(userTimezone);
      const futureTime = futureDate.format(profileTimeFormat);

      if (regularTime !== futureTime) {
        if (warnings.length > 0) {
          warnings[warnings.length - 1] +=
            ` until ${futureDate.clone().subtract(1, 'day').format('LL')}.`;
        }

        warnings.push(
          `Every ${futureDate.format('dddd')} at ${futureTime} (${futureDate.format('z')}) starting ${futureDate.format('LL')}`
        );
      }
    }

    if (warnings.length > 0) {
      warnings[warnings.length - 1] += ' onwards.';
    }

    return warnings;
  }

  static getUserTimezone(profile: Profile): string {
    if (moment.tz.zone(profile.timezone)) {
      return profile.timezone;
    }

    return moment.tz.guess();
  }

  static groupAppointments (
    appointments: Appointment[],
    userTimezone: string,
    profileTimeFormat: string
  ): {
    appointmentsByHour: { [key: string]: Appointment[] },
    appointmentsByDay: { [key: string]: Appointment[] },
    appointmentHours: string[]
  } {
    const appointmentsByHour = {};
    const appointmentsByDay = {};

    const localTimes: { hour, hourProfile }[] = [];
    appointments.map((appointment) => {
      const localTime = moment(appointment['nextDate']).tz(userTimezone);
      const localHour = localTime.format(`${profileTimeFormat || 'HH:mm'} z`);
      const localDay = localTime.weekday();

      if (!gv(appointmentsByHour, localHour, null)) {
        appointmentsByHour[localHour] = [];
        localTimes.push({
          hour: localTime.format('HHmm'),
          hourProfile: localHour
        });
      }

      if (!gv(appointmentsByDay, localDay, null)) {
        appointmentsByDay[localDay] = [];
      }

      appointmentsByHour[localHour].push({
        localHour,
        localDay,
        ...appointment
      });

      appointmentsByDay[localDay].push({
        localHourIndex: localTime.format('HHmm'), // use for sorting
        localHour,
        localDay,
        ...appointment
      });
    });

    // sort hours ascending and format
    const appointmentHours = sortBy(localTimes, 'hour')
      .map(({ hourProfile }) => hourProfile);

    for (const day of Object.keys(appointmentsByDay)) {
      appointmentsByDay[day] = sortBy(appointmentsByDay[day], 'localHourIndex')
        .map(({ localHourIndex, ...appointment }) => appointment);
    }

    return { appointmentsByHour, appointmentsByDay, appointmentHours };
  }

  static getCountdownForOwnAppointment(ownAppointment: Appointment): string {
    if (!ownAppointment) {
      return '';
    }

    const nextDate = ownAppointment['nextDate'];
    const diffMins = moment(nextDate).diff(moment(), 'minutes');

    if (diffMins === 0 || diffMins >= (60 * 24 * 7 - 5)) {
      return 'now';
    }

    const mins = diffMins % 60;
    let hours = Math.floor(diffMins / 60);
    const days = Math.floor(hours / 24);
    hours %= 24;

    return 'starts in ' + [`${days} day(s)`, `${hours} hour(s)`, `${mins} minute(s)`]
      .filter(x => !x.startsWith('0'))
      .join(', ');
  }

  static parseStringToDate(
    str: string,
    format = 'YYYY-MM-DD',
    timezone = 'utc'
  ): moment.Moment {
    return moment.tz(str, format, true, timezone);
  }
}
