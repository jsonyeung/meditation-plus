import { Action } from '@ngrx/store';
import { Appointment, OwnAppointmentStatus } from '../appointment';

export const CHECK_OWN_APPOINTMENT = '[Appointment] Check Own Appointment';
export const CHECK_OWN_APPOINTMENT_ERROR = '[Appointment] Check Own Appointment Error';
export const CHECK_OWN_APPOINTMENT_DONE = '[Appointment] Check Own Appointment Done';

export const RELOAD_APPOINTMENTS_TABLE = '[Appointment] Reload Appointments Table';

export const LOAD_APPOINTMENTS = '[Appointment] Load Appointments';
export const LOAD_APPOINTMENTS_ERROR = '[Appointment] Load Appointments Error';
export const LOAD_APPOINTMENTS_DONE = '[Appointment] Load Appointments Done';

export const SELECT_TEACHER = '[Appointment] Select Teacher';

export const SET_TIMEZONE = '[Appointment] Set Timezone';
export const SET_HOUR_FORMAT = '[Appointment] Set Hour Format';

export class CheckOwnAppointment implements Action {
  readonly type = CHECK_OWN_APPOINTMENT;
}

export class CheckOwnAppointmentError implements Action {
  readonly type = CHECK_OWN_APPOINTMENT_ERROR;
  constructor(public payload: Error) {}
}

export class CheckOwnAppointmentDone implements Action {
  readonly type = CHECK_OWN_APPOINTMENT_DONE;
  constructor(public payload: OwnAppointmentStatus) {}
}

export class LoadAppointments implements Action {
  readonly type = LOAD_APPOINTMENTS;
}

export class LoadAppointmentsError implements Action {
  readonly type = LOAD_APPOINTMENTS_ERROR;
  constructor(public payload: Error) {}
}

export class LoadAppointmentsDone implements Action {
  readonly type = LOAD_APPOINTMENTS_DONE;
  constructor(public payload: {
    teachers: object[],
    ownAppointment: Appointment,
    appointments: Appointment[]
  }) {}
}

export class ReloadAppointmentsTable implements Action {
  readonly type = RELOAD_APPOINTMENTS_TABLE;
}

export class SelectTeacher implements Action {
  readonly type = SELECT_TEACHER;
  constructor(public payload: string) {}
}

export class SetTimezone implements Action {
  readonly type = SET_TIMEZONE;
  constructor(public payload: string) {}
}

export class SetHourFormat implements Action {
  readonly type = SET_HOUR_FORMAT;
  constructor(public payload: string) {}
}

export type Actions = CheckOwnAppointment
  | CheckOwnAppointmentDone
  | CheckOwnAppointmentError
  | LoadAppointments
  | LoadAppointmentsDone
  | LoadAppointmentsError
  | SelectTeacher
  | SetTimezone
  | SetHourFormat
  | ReloadAppointmentsTable;
