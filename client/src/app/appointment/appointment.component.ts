import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { AppointmentService } from './appointment.service';
import { AppointmentUtils } from './appointment.utils';
import { SettingsService } from '../shared';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment-timezone';
import { Store } from '@ngrx/store';
import { SetTitle, ThrowError } from '../actions/global.actions';
import { AppState } from '../reducers';
import { MatTabGroup } from '@angular/material';
import { LoadAppointments } from './actions/appointment.actions';

@Component({
  selector: 'appointment',
  templateUrl: './appointment.component.html',
  styleUrls: [
    './appointment.component.styl'
  ]
})
export class AppointmentComponent implements OnInit, OnDestroy {

  @ViewChild(MatTabGroup) tabGroup: MatTabGroup;

  // appointment settings
  meetingPageDisabledFrom: string;
  meetingPageDisabledTo: string;
  meetingPageReactivatedAt: string;

  disableAppointmentsTimezone = '';
  disableAppointments = false;
  disableAppointmentsFrom: string;
  disableAppointmentsUntil: string;

  appointmentSocket;

  tabs = ['info', 'schedule'];
  currentTab = 'info';

  constructor(
    private appointmentService: AppointmentService,
    private settingsService: SettingsService,
    private route: ActivatedRoute,
    private router: Router,
    private store: Store<AppState>
  ) {
    this.loadSettings();

    store.dispatch(new SetTitle('At-Home Meditation Course'));
  }

  navigate(index: number) {
    this.router.navigate(['course', {tab: this.tabs[index]}]);
    this.tab(this.tabs[index]);
  }

  tab(tab: string) {
    let index = this.tabs.indexOf(tab);
    index = index > -1 ? index : 0;
    this.tabGroup.selectedIndex = index;
    this.currentTab = this.tabs[index];
  }

  /**
   * Loads the settings entity
   */
  loadSettings(): void {
    this.settingsService
      .get()
      .subscribe(({
        disableAppointments,
        disableAppointmentsTimezone,
        disableAppointmentsFrom,
        disableAppointmentsUntil
      }) => {
        this.disableAppointmentsTimezone = moment().tz(disableAppointmentsTimezone).format('z');

        if (
          disableAppointments
          && disableAppointmentsFrom
          && disableAppointmentsUntil
        ) {
          this.disableAppointmentsFrom = disableAppointmentsFrom;
          this.disableAppointmentsUntil = disableAppointmentsUntil;
          this.meetingPageDisabledFrom = AppointmentUtils
            .parseStringToDate(disableAppointmentsFrom, 'YYYY-MM-DD', disableAppointmentsTimezone)
            .format('MMMM Do YYYY z');
          this.meetingPageDisabledTo = AppointmentUtils
            .parseStringToDate(disableAppointmentsUntil, 'YYYY-MM-DD', disableAppointmentsTimezone)
            .format('MMMM Do YYYY z');
          this.meetingPageReactivatedAt = AppointmentUtils
            .parseStringToDate(disableAppointmentsUntil, 'YYYY-MM-DD', disableAppointmentsTimezone)
            .add(1, 'day')
            .format('MMMM Do YYYY z');
        }

        this.disableAppointments = disableAppointments;
      },
      error => this.store.dispatch(new ThrowError({ error }))
    );
  }

  ngOnInit() {
    this.route.paramMap.subscribe(res => this.tab(res.get('tab')));

    // initialize websocket for instant data
    this.appointmentSocket = this.appointmentService.getSocket()
      .subscribe(() => this.store.dispatch(new LoadAppointments()));
  }

  ngOnDestroy() {
    this.appointmentSocket.unsubscribe();
  }
}
