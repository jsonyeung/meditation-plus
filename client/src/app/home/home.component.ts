import { Component, ViewChild, OnInit } from '@angular/core';
import { MeditationComponent } from '../meditation';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../user/user.service';
import { MessageService } from '../message/message.service';
import { WebsocketService, SettingsService } from '../shared';
import * as moment from 'moment';
import { filter, map, concatMap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AppState } from '../reducers';
import { SetTitle, SetSidenavOpen } from '../actions/global.actions';
import { Observable, timer } from 'rxjs';
import { MatTabGroup } from '@angular/material';
import { MessageComponent } from '../message';
import { selectIsMeditating } from 'app/meditation/reducers/meditation.reducers';
import * as firebase from 'firebase/app';
import 'firebase/messaging';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: [
    './home.component.styl'
  ]
})
export class HomeComponent implements OnInit {
  @ViewChild(MeditationComponent) medComponent: MeditationComponent;
  @ViewChild(MessageComponent) messageComponent: MessageComponent;
  @ViewChild(MatTabGroup) tabGroup: MatTabGroup;

  currentTab = '';
  activated: string[] = [];
  isMeditating$: Observable<boolean>;
  newMessages = 0;
  notification$: Observable<string>;

  tabs = ['meditation', 'chat', 'ask'];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private messageService: MessageService,
    private wsService: WebsocketService,
    private store: Store<AppState>,
    private settings: SettingsService
  ) {
    this.store.dispatch(new SetTitle(''));
    this.store.dispatch(new SetSidenavOpen(false));

    this.isMeditating$ = this.store.select(selectIsMeditating);

    this.notification$ = timer(0, 10000).pipe(
      concatMap(() => this.settings.get()),
      map(val => val.notification)
    );
  }

  navigate(index: number) {
    this.router.navigate(['home', {tab: this.tabs[index]}]);
    this.tab(this.tabs[index]);
  }

  getButtonColor(tab: string) {
    return this.currentTab === tab ? 'primary' : '';
  }

  isCurrentTab(tab: string): boolean {
    return this.currentTab === tab;
  }

  tab(tab: string) {
    let index = this.tabs.indexOf(tab);
    index = index > -1 ? index : 0;
    this.tabGroup.selectedIndex = index;
    this.currentTab = this.tabs[index];

    if (this.tabs[index] === 'chat') {
      this.newMessages = 0;
      this.messageService.setLastMessage(moment().toISOString());

      if (this.messageComponent) {
        this.messageComponent.scrollToBottom();
      }
    }

    if (this.activated.indexOf(this.tabs[index]) < 0) {
      this.activated.push(this.tabs[index]);
    }
  }

  openSidenav() {
    this.store.dispatch(new SetSidenavOpen(true));
  }

  /**
   * Loads content with lower priority
   */
  afterLoadedComponent() {
    const lastMessageDate = this.messageService.getLastMessage();
    if (lastMessageDate) {
      // Get number of new messages
      this.messageService.synchronize(moment(lastMessageDate).utc(), moment().utc(), true)
        .subscribe(res => this.newMessages = res);
    }

    // Update message counter on new message
    this.wsService.onMessage()
      .pipe(
        filter(() => this.currentTab !== 'chat')
      )
      .subscribe(() => this.newMessages++);
  }

  ngOnInit() {
    this.route.paramMap
      .subscribe(res => this.tab(res.get('tab')));

    try {
      this.userService.initializeFirebase();

      if (firebase.apps.length) {
        const messaging = firebase.messaging();

        const registerToken = (token) => {
          const oldToken = localStorage.getItem('MEDITATION_PLUS_FCM_TOKEN');

          // no need to send token, because it was already registered
          if (token === oldToken) {
            return;
          }

          this.userService
            .registerPushSubscription(token, oldToken)
            .subscribe(
              () => localStorage.setItem('MEDITATION_PLUS_FCM_TOKEN', token),
              err => console.error(err)
            );
        };

        messaging.requestPermission()
          .then(() => messaging.getToken().then(token => registerToken(token)))
          .catch((err) => console.error(err));

        messaging.onTokenRefresh(() => messaging.getToken()
          .then(token => registerToken(token))
          .catch((err) => console.error(err))
        );
      }
    } catch (err) {
      console.error('Could not initialize FCM', err);
    }
  }
}
