import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LinkyModule } from 'angular-linky';
import { WebsocketService } from './websocket.service';
import { MaterialModule } from './material.module';
import { AuthService } from './auth.service';
import { AdminGuard } from './admin-guard';
import { LoginGuard } from './login-guard';
import { AuthGuard } from './auth-guard';
import { TeacherGuard } from './teacher-guard';
import { FormatHourPipe } from './hour.pipe';
import { ErrorHandlerService } from './error-handler.service';
import { AvatarDirective } from '../profile/avatar.directive';
import { AdminSectionGuard } from './admin-section-guard';
import { MomentFormatPipe } from './moment-format.pipe';
import { UserAvatarComponent } from './user-avatar/user-avatar.component';
import { AsyncButtonComponent } from './async-button/async-button.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    LinkyModule
  ],
  providers: [
    WebsocketService,
    ErrorHandlerService,
    AuthService,
    AuthGuard,
    LoginGuard,
    AdminGuard,
    TeacherGuard,
    AdminSectionGuard
  ],
  declarations: [
    FormatHourPipe,
    AvatarDirective,
    MomentFormatPipe,
    UserAvatarComponent,
    AsyncButtonComponent
  ],
  exports: [
    CommonModule,
    MaterialModule,
    LinkyModule,
    FormatHourPipe,
    MomentFormatPipe,
    AvatarDirective,
    UserAvatarComponent,
    AsyncButtonComponent,
  ]
})
export class SharedModule { }
