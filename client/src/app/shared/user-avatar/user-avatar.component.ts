import { Component, Input, OnInit } from '@angular/core';
import { UserAvatar } from './user-avatar';
import { User } from './user';
import { UserAvatarSize } from './user-avatar-size';

@Component({
  selector: 'user-avatar',
  templateUrl: './user-avatar.component.html',
  styleUrls: ['./user-avatar.component.styl']
})
export class UserAvatarComponent implements OnInit {
  @Input() user?: User;
  @Input() thumbnailSize: UserAvatarSize = UserAvatarSize.Lg;

  userAvatar: UserAvatar;

  constructor() { }

  ngOnInit() {
    if (this.user) {
      this.userAvatar = new UserAvatar(this.user);
    }
  }
}
