export enum UserAvatarSize {
  Sm = 'sm',
  Md = 'md',
  Lg = 'lg',
}
