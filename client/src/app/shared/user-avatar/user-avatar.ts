import { User } from './user';

export class UserAvatar {
  username: string;
  name: string;
  avatarImageToken?: string;
  backgroundColor?: string;
  initials?: string;

  constructor(user: User) {
    this.username = user.username;
    this.name = user.name;

    if (user.avatarImageToken) {
      this.avatarImageToken = user.avatarImageToken;
      return;
    }

    this.backgroundColor = UserAvatar.getBackgroundColor(user.username || user.name);
    this.initials = UserAvatar.getInitials(user.name);
  }

  static getBackgroundColor(username: string): string {
    let hash = 0;
    const saturation = 30;
    const lightness = 60;

    for (let i = 0; i < username.length; i++) {
      /* tslint:disable:no-bitwise */
      hash = username.charCodeAt(i) + ((hash << 5) - hash);
      /* tslint:enable:no-bitwise */
    }

    let h = hash % 360;
    h = h < 0 ? h + 360 : h;

    return `hsl(${h},${saturation}%, ${lightness}%)`;
  }

  static getInitials(fullName: string): string {
    return fullName.split(' ')
      .filter((str) => str)
      .reduce((result, name, index, arr) => {
          if (index === 0 || index === arr.length - 1) {
              result = result.concat(name.charAt(0).toUpperCase());
          }

          return result;
      }, '');
  }
}
