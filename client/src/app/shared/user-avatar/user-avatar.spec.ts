import { User } from './user';
import { UserAvatar } from './user-avatar';

describe('UserAvatar with image token', () => {
    let user: User;

    beforeEach(() => {
        user = {
           _id: 'id',
           name: 'John Doe',
           username: 'JDMan',
           avatarImageToken: 'imageToken'
        };

    });

    it('should set the name, _id, username and avatarImageToken', () => {
        const userAvatar = new UserAvatar(user);

        expect(userAvatar.name).toBe('John Doe');
        expect(userAvatar.username).toBe('JDMan');
        expect(userAvatar.avatarImageToken).toBe('imageToken');
    })

    it('should not set background color or initial if user has an image token', () => {
        const userAvatar = new UserAvatar(user);

        expect(userAvatar.initials).toBeUndefined();
        expect(userAvatar.backgroundColor).toBeUndefined();
    });
});

describe('UserAvatar without image token', () => {
    let user: User;

    beforeEach(() => {
        user = {
           _id: 'id',
           name: 'John Doe',
           username: 'JDMan',
        };
    });

    it('should set the proper initials', () => {
        expect(new UserAvatar(user).initials).toBe('JD');

        user.name = 'John D Doe Man';
        expect(new UserAvatar(user).initials).toBe('JM');

        user.name = 'John D    Doe Can   ';
        expect(new UserAvatar(user).initials).toBe('JC');

        user.name = ' @#John D    Doe Can   ';
        expect(new UserAvatar(user).initials).toBe('@C');
    });

    it('should set a background color for the user', () => {
        expect(new UserAvatar(user).backgroundColor).toBeTruthy();
    });
});