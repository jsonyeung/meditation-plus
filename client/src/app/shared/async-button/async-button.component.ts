import { Component, Input } from '@angular/core';

@Component({
  selector: 'async-button',
  templateUrl: './async-button.component.html',
  styleUrls: ['./async-button.component.styl'],
})
export class AsyncButtonComponent {
  @Input() color: string;
  @Input() loading: boolean;
  @Input() disabled?: boolean;

  constructor() {}
}
