import { Injectable } from '@angular/core';
import { ApiConfig } from '../../api.config';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class TestimonialService {

  public constructor(private http: HttpClient) {
  }

  public getAll(): Observable<any> {
    return this.http.get(ApiConfig.url + '/api/testimonial');
  }

  public getAllAdmin(): Observable<any> {
    return this.http.get(ApiConfig.url + '/api/testimonial/admin');
  }

  public post(testimonial: string, anonymous: boolean): Observable<any> {
    return this.http.post(
      ApiConfig.url + '/api/testimonial',
      JSON.stringify({ text: testimonial, anonymous: anonymous })
    );
  }

  public toggleReviewed(testimonialId: string): Observable<any> {
    return this.http.put(
      ApiConfig.url + '/api/testimonial/review',
      JSON.stringify({ id : testimonialId })
    );
  }

  public delete(testimonial): Observable<any> {
    return this.http.delete(ApiConfig.url + '/api/testimonial/' + testimonial._id);
  }

}
