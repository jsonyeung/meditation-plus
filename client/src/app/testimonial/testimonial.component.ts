import {
  Component,
  ViewChild,
  ElementRef,
  ApplicationRef
} from '@angular/core';
import { TestimonialService } from './testimonial.service';
import { MatBottomSheet, MatSnackBar } from '@angular/material';
import { EmojiSelectComponent } from 'app/emoji';
import { filter } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AppState } from '../reducers';
import { SetTitle, ThrowError } from '../actions/global.actions';

@Component({
  selector: 'testimonials',
  templateUrl: './testimonial.component.html',
  styleUrls: [
    './testimonial.component.styl'
  ]
})
export class TestimonialComponent {

  @ViewChild('testimonialsList', {read: ElementRef}) testimonialsList: ElementRef;

  testimonials: any[];
  allowUser = true;
  showForm = false;
  currentTestimonial = '';
  currentIsAnonymous = false;
  lastScrollTop = 0;
  lastScrollHeight = 0;
  loading = false;

  constructor(
    private testimonialService: TestimonialService,
    private appRef: ApplicationRef,
    private sheet: MatBottomSheet,
    private snackbar: MatSnackBar,
    private store: Store<AppState>
  ) {
    this.store.dispatch(new SetTitle('Testimonials'));
    this.loadTestimonials();
  }

  scroll() {
    this.lastScrollHeight = this.testimonialsList.nativeElement.scrollHeight;
    this.lastScrollTop = this.testimonialsList.nativeElement.scrollTop;
  }

  loadTestimonials() {
    this.testimonialService.getAll()
      .subscribe(data => {
        this.testimonials = data.testimonials;
        this.allowUser = data.allowUser;
        this.appRef.tick();
      });
  }

  sendTestimonial(evt) {
    evt.preventDefault();

    if (!this.currentTestimonial) {
      return;
    }

    this.loading = true;

    this.testimonialService.post(this.currentTestimonial, this.currentIsAnonymous)
      .subscribe(
        () => {
          this.currentTestimonial = '';
          this.currentIsAnonymous = false;
          this.snackbar.open('Thank you for your testimonial. It will be added after it has been reviewed.');
          this.toggleShowForm();
        },
        error => this.store.dispatch(new ThrowError({ error })),
        () => this.loading = false
      );
  }

  toggleShowForm(evt?) {
    if (evt) {
      evt.preventDefault();
    }
    this.showForm = !this.showForm;
  }

  showEmojiSelect() {
    this.sheet.open(EmojiSelectComponent).afterDismissed()
      .pipe(filter(val => !!val))
      .subscribe(val => this.currentTestimonial += `:${val}:`);
  }
}
