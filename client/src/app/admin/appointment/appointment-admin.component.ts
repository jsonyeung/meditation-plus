import { Component } from '@angular/core';
import { AppointmentService } from '../../appointment';
import { SettingsService } from '../../shared';
import * as moment from 'moment-timezone';
import { AppointmentHourVO } from '../../appointment/appointment';
import { tap, filter, take } from 'rxjs/operators';
import { MatSelectChange, MatSnackBar } from '@angular/material';
import { Store } from '@ngrx/store';
import { AppState } from '../../reducers';
import { DialogService } from '../../dialog/dialog.service';
import { ThrowError } from '../../actions/global.actions';
import { selectAdmin, selectId } from 'app/auth/reducers/auth.reducers';
import { Observable } from 'rxjs';
import { find } from 'lodash';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'appointment-admin',
  templateUrl: './appointment-admin.component.html',
  styleUrls: [
    './appointment-admin.component.styl'
  ]
})
export class AppointmentAdminComponent {
  initialLoading = true;
  confirmDeletions = true;

  allTeachers = [];

  teacherId = '';

  // appointment data + feedback flags
  appointments = new Map<string, AppointmentHourVO>();
  keys: string[] = [];
  weekdays: string[] = moment.weekdays();

  timezone = '';
  timezones = moment.tz.names();
  // define standard timezone until 'timezone' is loaded
  zoneName = '';

  disabled: boolean;
  dateFrom: string;
  dateTo: string;
  disableAppointmentsError: string;
  disableAppointmentsTimezone = '';

  // notification status
  tickerSubscribed: boolean;
  tickerLoading: boolean;
  settings;

  registrationForerun: number;
  toEarlyTolerance: number;
  toLateTolerance: number;

  userId$: Observable<string>;
  admin$: Observable<boolean>;

  constructor(
    private appointmentService: AppointmentService,
    private settingsService: SettingsService,
    private snackbar: MatSnackBar,
    private store: Store<AppState>,
    private dialog: DialogService
  ) {
    this.loadSettings();
    this.userId$ = store.select(selectId);
    this.admin$ = store.select(selectAdmin);

    this.appointmentService.listTeachers()
      .subscribe(teachers => {
        this.allTeachers = teachers;

        // if the current user is a teacher,
        // then automatically set dropdown selected table to own one
        this.userId$
          .pipe(
            take(1),
            filter(id => !!find(
                this.allTeachers,
                ({ _id }) => _id.toString() === id.toString()
              )
            )
          )
          .subscribe(
            id => {
              this.teacherId = id;
              this.loadAppointments();
            },
            err => console.error(err)
          );
      });

  }

  selectTeacher(e: MatSelectChange) {
    if (!e.value) {
      return;
    }

    this.teacherId = e.value;
    this.initialLoading = true;
    this.loadAppointments();
  }

  /**
   * Loads all appointments
   */
  loadAppointments(): void {
    this.appointmentService
      .getAggregated(this.teacherId)
      .pipe(
        tap(() => this.initialLoading = false)
      )
      .subscribe(res => {
        this.timezone = res['teacher']['timezone'];
        this.zoneName = moment.tz(this.timezone).zoneName();

        const prevMap = new Map(this.appointments);
        this.appointments = new Map();

        for (const hour of res['appointments']) {
          this.appointments.set(hour._id, {
            ...prevMap.get(hour._id) || {},
            ...hour
          });
        }

        this.keys = Array.from(this.appointments.keys()).sort((a, b) => parseInt(a, 10) - parseInt(b, 10));
      }, (err: HttpErrorResponse) => {
        this.initialLoading = false;

        console.error(err);

        this.dialog.alert('An error occurred', err.error);
      });
  }

  /**
   * Loads the settings entity
   */
  loadSettings(): void {
    this.settingsService
      .get()
      .subscribe(res => {
        this.settings = res;
        this.disabled = res.disableAppointments;
        this.dateFrom = res.disableAppointmentsFrom;
        this.dateTo = res.disableAppointmentsUntil;
        this.registrationForerun = res.appointmentsRegistrationForerun;
        this.toEarlyTolerance = res.appointmentsToEarlyTolerance;
        this.toLateTolerance = res.appointmentsToLateTolerance;
        this.disableAppointmentsTimezone = res.disableAppointmentsTimezone;
      },
      error => this.store.dispatch(new ThrowError({ error }))
    );
  }

  /**
   * Submit the date interval for which appointments are disabled
   *
   * @param disabled
   * @param dateFrom
   * @param dateTo
   */
  submitDisabledDates(disabled: boolean, dateFrom: string, dateTo: string): void {
    this.settingsService.setAppointmentsDisabled(disabled, dateFrom, dateTo)
      .subscribe(
        () => {
          this.disabled = disabled;
          this.snackbar.open('The settings have been updated successfully.');
          this.disableAppointmentsError = '';
          this.loadSettings();
        },
        err => this.disableAppointmentsError = err.error.error
      );
  }

  toggleDisableSchedule(): void {
    if (this.disabled) {
      this.submitDisabledDates(false, '', '');
    } else {
      this.dialog.confirm(
        'Warning',
        'By confirming you disable the schedule for all users. Are you sure?'
      ).pipe(filter(val => !!val)).subscribe(() => this.submitDisabledDates(true, '', ''));
    }
  }

  /**
   * Show the edit form for one specific card.
   *
   * @param hour The hour to identify the card
   */
  toggleEdit(hour: string): void {
    if (!this.appointments.has(hour)) {
      return;
    }
    const app = this.appointments.get(hour);
    app.status = app.status === 'editing' ? null : 'editing';
  }

  /**
   * Toggle the existence of an appointment (via checkbox in the interface).
   *
   * @param hour Hour of appointment
   * @param day Weekday of appointment; '-1' tells the server to delete the selected hour for all days
   */
  toggleDay(evt: Event, hour: string, day: number): void {
    evt.preventDefault();

    if (!this.appointments.has(hour)) {
      return;
    }

    const appointment = this.appointments.get(hour);

    if (this.confirmDeletions && (appointment.days.includes(day) || day === -1)) {
      this.dialog.confirm('Confirmation', 'Are you sure?')
        .pipe(filter(val => !!val))
        .subscribe(() => this.doToggleDay(appointment, hour, day));
      return;
    }

    this.doToggleDay(appointment, hour, day);
  }

  private doToggleDay(appointment: AppointmentHourVO, hour: string, day: number) {
    if (!this.teacherId) {
      return;
    }

    // reset user feedback
    appointment.status = 'loading';

    this.appointmentService
      .toggle(this.teacherId, parseInt(hour, 10), day)
      .subscribe(
        () => appointment.status = 'success',
        err => {
          appointment.errorMessage = err.error;
          appointment.status = 'error';
        },
        () => this.loadAppointments()
      );
  }

  /**
   * Update appointments hours.
   *
   * @param evt         An event
   * @param oldHour     Hour to identify appointments
   * @param newHourStr  New hour after update as HH:mm formatted string
   */
  updateHour(evt: Event, oldHour: any, newHourStr: string): void {
    if (evt) {
      evt.preventDefault();
    }

    oldHour = parseInt(oldHour, 10);
    const newHour = parseInt(newHourStr.replace(':', ''), 10);

    if (isNaN(oldHour) || isNaN(newHour) || !this.appointments.has(oldHour)) {
      return;
    }

    const appointment = this.appointments.get(oldHour);
    appointment.status = 'editLoading';
    this.appointmentService
      .update(this.teacherId, oldHour, newHour)
      .subscribe(
        () => {
          this.snackbar.open('The appointment has been updated successfully.');
          this.loadAppointments();
        },
        err => {
          appointment.errorMessage = err.error;
          appointment.status = 'error';
        }
      );
  }

  /**
   * Updates the value of the global
   * settings entity
   */
  updateSettings(key: string, value: any) {
    if (!key || typeof value === 'undefined') {
      return;
    }
    // update value in settings
    this.settingsService
      .set(key, value)
      .subscribe(() => {
        this.snackbar.open('The settings have been updated successfully.');
        this.loadSettings();
      },
      error => this.store.dispatch(new ThrowError({ error }))
    );
  }

  /**
   * Converts number representing hour (like 1300) to
   * string in the time format HH:mm ('13:00')
   */
  formatHour(hour: number): string {
    const hourStr = '0000' + hour.toString();
    return hourStr.substr(-4, 2) + ':' + hourStr.substr(-2, 2);
  }
}
