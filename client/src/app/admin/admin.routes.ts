import { CommitmentAdminComponent } from './commitment/commitment-admin.component';
import { CommitmentFormComponent } from './commitment/commitment-form.component';
import { AppointmentAdminComponent } from './appointment/appointment-admin.component';
import { AppointmentFormComponent } from './appointment/appointment-form.component';
import { UserAdminFormComponent } from './user/user-admin-form.component';
import { UserAdminComponent } from './user/user-admin.component';
import { FeedbackAdminComponent } from './feedback/feedback-admin.component';
import { TestimonialAdminComponent } from './testimonial/testimonial-admin.component';
import { BroadcastAdminComponent } from './broadcast/broadcast-admin.component';
import { BroadcastFormComponent } from './broadcast/broadcast-form.component';
import { AnalyticsComponent } from './analytics/analytics.component';
import { AdminIndexComponent } from './admin-index.component';
import { Routes } from '@angular/router';
import { VideoSuggestionsComponent } from './video-suggestions/video-suggestions.component';
import { AdminComponent } from './admin.component';
import { AdminGuard } from 'app/shared/admin-guard';
import { AdminSectionGuard } from 'app/shared/admin-section-guard';

export const adminRoutes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      { path: '', component: AdminIndexComponent, canActivate: [AdminSectionGuard] },
      { path: 'appointments', component: AppointmentAdminComponent, canActivate: [AdminSectionGuard] },
      { path: 'appointments/:teacher', component: AppointmentAdminComponent, canActivate: [AdminSectionGuard] },
      { path: 'appointments/:teacher/new', component: AppointmentFormComponent, canActivate: [AdminSectionGuard] },
      { path: 'broadcasts', component: BroadcastAdminComponent, canActivate: [AdminGuard] },
      { path: 'broadcasts/new', component: BroadcastFormComponent, canActivate: [AdminGuard] },
      { path: 'broadcasts/:id', component: BroadcastFormComponent, canActivate: [AdminGuard] },
      { path: 'commitments', component: CommitmentAdminComponent, canActivate: [AdminGuard] },
      { path: 'commitments/new', component: CommitmentFormComponent, canActivate: [AdminGuard] },
      { path: 'commitments/:id', component: CommitmentFormComponent, canActivate: [AdminGuard] },
      { path: 'feedback', component: FeedbackAdminComponent, canActivate: [AdminGuard] },
      { path: 'users', component: UserAdminComponent, canActivate: [AdminGuard] },
      { path: 'users/new', component: UserAdminFormComponent, canActivate: [AdminGuard] },
      { path: 'users/:id', component: UserAdminFormComponent, canActivate: [AdminGuard] },
      { path: 'testimonials', component: TestimonialAdminComponent, canActivate: [AdminGuard] },
      { path: 'testimonials/:id', component: TestimonialAdminComponent, canActivate: [AdminGuard] },
      { path: 'testimonials', component: TestimonialAdminComponent, canActivate: [AdminGuard] },
      { path: 'testimonials/:id', component: TestimonialAdminComponent, canActivate: [AdminGuard] },
      { path: 'testimonials/review', component: TestimonialAdminComponent, canActivate: [AdminGuard] },
      { path: 'analytics', component: AnalyticsComponent, canActivate: [AdminGuard] },
      { path: 'video-suggestions', component: VideoSuggestionsComponent, canActivate: [AdminGuard] }
    ]
  }
];
