import { Injectable } from '@angular/core';
import { ApiConfig } from '../../../api.config';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class BroadcastService {

  public constructor(private http: HttpClient) {
  }

  public getAll(): Observable<any[]> {
    return this.http.get<any[]>(ApiConfig.url + '/api/broadcast');
  }

  public get(id: string): Observable<any> {
    return this.http.get(ApiConfig.url + '/api/broadcast/' + id);
  }

  public save(broadcast): Observable<any> {
    return this.http.put(
      ApiConfig.url + '/api/broadcast/' + broadcast._id,
      broadcast,
      {
        responseType: 'text'
      }
    );
  }

  public delete(broadcast): Observable<any> {
    return this.http.delete(ApiConfig.url + '/api/broadcast/' + broadcast._id);
  }
}
