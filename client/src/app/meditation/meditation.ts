export interface Meditation {
  _id: string;
  walking: number;
  sitting: number;
  numOfLikes: number;
  user: any;
  sittingLeft?: number;
  walkingLeft?: number;
  status?: 'walking' | 'sitting' | 'done';
  createdAt?: Date;
  updatedAt?: Date;
  end?: Date;
}
