import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { catchError, concatMap, map, tap, withLatestFrom, mergeMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { MeditationService } from '../meditation.service';
import {
  LoadMeditations,
  LOAD,
  LoadMeditationsDone,
  LoadMeditationsError,
  LOAD_DONE,
  POST,
  PostMeditation,
  WebsocketOnMeditation,
  WS_ON_MEDITATION,
  CancelMeditation,
  CancelMeditationDone,
  CANCEL,
  CANCEL_DONE,
  LIKE,
  LikeMeditationsDone,
  LikeMeditations,
  PostMeditationDone,
  POST_DONE
} from '../actions/meditation.actions';
import { SetTitle } from 'app/actions/global.actions';
import { Store } from '@ngrx/store';
import { AppState } from '../../reducers';
import { selectId } from '../../auth/reducers/auth.reducers';
import { ReloadProfile } from '../../auth/actions/auth.actions';

@Injectable()
export class MeditationEffect {
  constructor(
    private actions$: Actions,
    private service: MeditationService,
    private store: Store<AppState>
  ) {
  }

  @Effect()
  load$ = this.actions$
    .ofType<LoadMeditations>(LOAD)
    .pipe(
      withLatestFrom(this.store.select(selectId)),
      concatMap(([action, userId]) =>
        this.service.getRecent().pipe(
          concatMap(meditations => of(new LoadMeditationsDone({ meditations, currentUserId: userId }))),
          catchError(() => of(new LoadMeditationsError()))
        )
      )
    );

  @Effect()
  loadDone$ = this.actions$
    .ofType<LoadMeditationsDone>(LOAD_DONE)
    .pipe(
      map(action => {
        const meditation = action.payload.meditations.find(data =>
          data.user._id === action.payload.currentUserId && (data.walkingLeft + data.sittingLeft) > 0
        );

        return new SetTitle(
          meditation
          ? (meditation.status === 'walking' ? 'Walking' : 'Sitting') +
            ' Meditation (' + (meditation.walkingLeft ? meditation.walkingLeft : meditation.sittingLeft) + 'm left)'
          : ''
        );
      })
    );

  @Effect()
  post$ = this.actions$
    .ofType<PostMeditation>(POST)
    .pipe(
      map(action => action.payload),
      tap(payload => {
        // saves last meditation data to localStorage
        window.localStorage.setItem('lastWalking', '' + payload.walking);
        window.localStorage.setItem('lastSitting', '' + payload.sitting);
      }),
      concatMap(payload =>
        this.service.post(payload.walking, payload.sitting).pipe(
          concatMap(() => of(new PostMeditationDone()))
        )
      )
    );

  @Effect()
  postDone$ = this.actions$
    .ofType<PostMeditationDone>(POST_DONE)
    .pipe(map(() => new LoadMeditations()));

  @Effect()
  cancel$ = this.actions$
    .ofType<CancelMeditation>(CANCEL)
    .pipe(
      concatMap(() =>
        this.service.stop().pipe(
          concatMap(() => of(new CancelMeditationDone()))
        )
      )
    );

  @Effect({ dispatch: false })
  cancelDone$ = this.actions$
    .ofType<CancelMeditationDone>(CANCEL_DONE);

  @Effect()
  like$ = this.actions$
    .ofType<LikeMeditations>(LIKE)
    .pipe(
      concatMap(() =>
        this.service.like().pipe(
          mergeMap(() => [
            new LikeMeditationsDone(),
            new ReloadProfile()
          ])
        )
      )
    );

  @Effect()
  socket$ =  this.service.getSocket()
    .pipe(map(() => new WebsocketOnMeditation()));

  @Effect()
  wsOnMeditation$ = this.actions$
    .ofType<WebsocketOnMeditation>(WS_ON_MEDITATION)
    .pipe(map(() => new LoadMeditations()));
}
