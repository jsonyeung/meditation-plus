import { Component, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { filter, concatMap } from 'rxjs/operators';
import { Country } from '../profile/country';
import * as moment from 'moment-timezone';
import { DialogService } from '../dialog/dialog.service';
import { UserService } from './user.service';
import { AuthService } from '../shared/auth.service';
import { get as gv } from 'lodash';

@Component({
  selector: 'user-form',
  templateUrl: './user-form.component.html',
  styleUrls: [
    './user-form.component.styl'
  ]
})
export class UserFormComponent implements OnChanges {

  @Input() model: any = {};
  @Output() modelChange: EventEmitter<any> = new EventEmitter<any>();
  @Input() admin = false;

  teacher = false;

  timezones = moment.tz.names();

  countryList = Country.list;
  sounds: Object[] = [
    { name: 'Bell 1', src: '/assets/audio/bell.mp3'},
    { name: 'Bell 2', src: '/assets/audio/bell1.mp3'},
    { name: 'Birds', src: '/assets/audio/birds.mp3'},
    { name: 'Bowl', src: '/assets/audio/bowl.mp3'},
    { name: 'Gong', src: '/assets/audio/gong.mp3'},
    { name: 'Notification Sound', src: '/assets/audio/solemn.mp3'}
  ];
  currentSound;

  constructor(
    private dialog: DialogService,
    private userService: UserService
  ) {}

  ngOnChanges() {
    this.teacher = [AuthService.teacherAdminRole, AuthService.teacherRole]
      .includes(gv(this.model, 'role', ''));
  }

  playSound() {
    if (this.model.sound) {
      this.currentSound = new Audio(this.model.sound);
      this.currentSound.play();
    }
  }

  stopSound() {
    if (this.currentSound) {
      this.currentSound.pause();
      this.currentSound.currentTime = 0;
    }
  }

  detectTimezone() {
    const guessedTz = moment.tz.guess();

    if (guessedTz) {
      this.model.timezone = guessedTz;
    } else {
      this.dialog.alert('Error', 'Your timezone was not detected.');
    }
  }

  uploadAvatar(evt = null) {
    if (evt) {
      evt.preventDefault();
    }

    this.dialog.uploadAvatar(this.admin && this.model._id)
      .pipe(filter(val => val && val.length > 0))
      .subscribe(val => this.model.avatarImageToken = val);
  }

  deleteAvatar(evt = null) {
    if (evt) {
      evt.preventDefault();
    }

    this.dialog.confirmDelete().pipe(
      filter(val => !!val),
      concatMap(() => this.userService.deleteAvatar(this.model._id))
    ).subscribe(() => this.model.avatarImageToken = '');
  }
}
