import { environment } from './environments/environment';

export class ApiConfig {
  public static url = environment.apiUrl;
  public static publicVapidKey = environment.publicVapidKey;
  public static fcm = environment.fcm;
}
