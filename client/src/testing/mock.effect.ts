import { of } from 'rxjs';

export class MockEffect {
  $autocomplete = of();
  cancelDone$ = of();
  postDone$ = of();
}
