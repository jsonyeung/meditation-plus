FROM node:14-alpine

ENV DISPOSABLE_DOMAINS_URL "https://raw.githubusercontent.com/ivolo/disposable-email-domains/master/index.json"
ENV STOPFORUM_SPAM_URL_PARTIAL "https://www.stopforumspam.com/downloads/toxic_domains_partial.txt"
ENV STOPFORUM_SPAM_URL_WHOLE "https://www.stopforumspam.com/downloads/toxic_domains_whole.txt"

WORKDIR /usr/src/app

COPY package.json yarn.lock ./
RUN yarn --frozen-lockfile --prod
COPY src ./src
COPY src/app/helper/reserved-usernames.json ./src/app/helper
COPY src/app/helper/mail-templates ./src/app/helper/mail-templates
COPY src/scripts/dev-data ./src/scripts/dev-data
RUN wget -O ./src/config/disposable-provider.json ${DISPOSABLE_DOMAINS_URL}
RUN wget -O ./src/config/banned-hosts-partial.txt ${STOPFORUM_SPAM_URL_PARTIAL}
RUN wget -O ./src/config/banned-hosts-whole.txt ${STOPFORUM_SPAM_URL_WHOLE}

RUN addgroup -S medplus && adduser -S -G medplus medplus
USER medplus

EXPOSE 3000

CMD ["node", "src/server.js"]