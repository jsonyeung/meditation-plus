import { logger } from '../app/helper/logger.js';
import path from 'path';
import { readFileSync, existsSync } from 'fs';

export default (firebase) => {
  if (process.env.NODE_ENV === 'test') {
    return;
  }

  const firebaseConfigPath = path.resolve(process.cwd(), '/src/config/firebase.json');

  if (!existsSync(firebaseConfigPath) && !process.env.FIREBASE_CONFIG) {
    logger.warn('No firebase config found. Skipping.');
    return;
  }

  const firebaseConfigFileContent = process.env.FIREBASE_CONFIG || readFileSync(firebaseConfigPath);

  try {
    const firebaseConfig = JSON.parse(firebaseConfigFileContent);
    firebase.initializeApp({
      credential: firebase.credential.cert(firebaseConfig),
      databaseURL: process.env.FIREBASE_DB
    });
  } catch (err) {
    logger.error('Could not initialize firebase', err);
  }
};
