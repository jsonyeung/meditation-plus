import fs from 'fs';

const UPLOAD_DIR = process.env.STORAGE_DIR + '/upload';
const PUBLIC_DIR = process.env.STORAGE_DIR + '/public';

function createDirectories() {
  [UPLOAD_DIR, PUBLIC_DIR]
    .filter(dir => !fs.existsSync(dir))
    .forEach(dir => fs.mkdirSync(dir, { recursive: true }));
}

export { UPLOAD_DIR, PUBLIC_DIR, createDirectories };
