import Appointment from '../models/appointment.model.js';
import { User } from '../models/user.model.js';
import { APPOINTMENT_EVENTS, AppointmentEvent } from '../models/appointment-event.model.js';
import Settings from '../models/settings.model.js';
import { logger, alertDiscord } from '../helper/logger.js';
import {
  findCurrentAppointmentForUser,
  isScheduleDisabled,
  formatHour,
  checkTeacherParam
} from '../helper/appointment.js';
import moment from 'moment-timezone';
import { get as gv } from 'lodash-es';
import mail from '../helper/mail.js';
import { QUERY_TEACHER } from '../constants.js';
import { authTeacher, isAdmin, isTeacher } from '../helper/auth.js';

import mongoose from 'mongoose';
let ObjectId = mongoose.Types.ObjectId;

export default (app, router, io) => {

  /**
   * @api {get} /api/appointment/teachers Get a list of all teachers
   * @apiName ListAppointmentTeachers
   * @apiGroup appointment
   *
   * @apiSuccess {Object[]} teachers             Array of objects with user information
   */
  router.get('/api/appointment/teachers', async (req, res) => {
    try {
      const teachers = await User
        .find(QUERY_TEACHER)
        .select('username name avatarImageToken timezone country')
        .lean()
        .exec();

      res.json(teachers);
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).send(err);
    }
  });

  /**
   * @api {get} /api/appointment Get appointment data
   * @apiName ListAppointment
   * @apiGroup appointment
   *
   * @apiSuccess {Object[]} appointments             List of all appointment slots
   * @apiSuccess {Number}   appointments.hour        Hour of day (in the teacher's timezone)
   * @apiSuccess {Number}   appointments.weekDay     Number of the weekday (in the teacher's timezone)
   * @apiSuccess {String}   appointments.nextDate    The next real date of the appointment
   * @apiSuccess {Object}   appointments.user        The meditating user
   * @apiSuccess {Object}   appointments.teacher     The meditation teacher's User-ID
   * @apiSuccess {Object[]} teachers                 A list of all teachers
   * @apiSuccess {Object}   ownAppointment           The appointment booked by the requesting user
   */
  router.get('/api/appointment', async (req, res) => {
    try {
      let json = {
        ownAppointment: null,
        teachers: [],
        appointments: []
      };

      json.teachers = await User
        .find(QUERY_TEACHER)
        .select('_id name username timezone country avatarImageToken')
        .sort('name')
        .lean()
        .exec();


      json.appointments = await Appointment
        .find()
        .populate('user', 'username name avatarImageToken timezone country')
        .lean()
        .exec();

      let teachersById = {};
      json.teachers.map(teacher => teachersById[teacher._id.toString()] = teacher);

      // Fill 'nextDate' property
      for (const appointment of json.appointments) {
        const teacher = gv(teachersById, appointment.teacher, '');

        if (!teacher) {
          appointment.nextDate = '';
          continue;
        }

        const now = moment.tz(teacher['timezone']);

        let nextDate = now.clone().set({
          weekday: appointment.weekDay,
          hour: Math.floor(appointment.hour / 100),
          minute: appointment.hour % 100,
          second: 0
        });

        if (nextDate < now) {
          nextDate = nextDate.add(7, 'days');
        }

        appointment.nextDate = nextDate.format();

        const isOwnAppointment = gv(appointment, 'user._id', '').toString() === req.user._id.toString();
        if (isOwnAppointment) {
          json.ownAppointment = appointment;
        }

        if (appointment.user && appointment.user._id) {
          appointment.user = (!isTeacher(req.user) && !isAdmin(req.user) && !isOwnAppointment)
            ? { hidden: true }
            : { ...appointment.user, hidden: false };
        }
      }

      res.json(json);
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).send(err);
    }
  });

  /**
   * @api {get} /api/appointment/meeting Get status of own appointment.
   * @apiName GetMeetingStatus
   * @apiGroup appointment
   *
   * @apiSuccess {Object}   status                    The status object
   * @apiSuccess {boolean}  status.isActive           Whether appointment is now
   * @apiSuccess {string}   status.callInformation    Information to initiate call (if active only)
   */
  router.get('/api/appointment/meeting', async (req, res) => {
    try {
      if (await isScheduleDisabled()) {
        return res.json({ isActive: false });
      }

      // try to create a new meeting for current appointment if possible otherwise
      const ongoingAppointment = await findCurrentAppointmentForUser(req.user);

      if (!ongoingAppointment) {
        return res.json({ isActive: false });
      }

      const teacher = await User.findById(ongoingAppointment.teacher)
        .select('local.skype')
        .lean();

      const callInformation = gv(teacher, 'local.skype', '');

      res.json({ isActive: true, callInformation });
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).json({ error: 'Internal Server Error' });
    }
  });

  /**
   * @api {get} /api/appointment/aggregated/:teacherId Get appointment days aggregated by their hour for specific teacher
   * @apiName AggregateAppointment
   * @apiGroup appointment
   */
  router.get('/api/appointment/aggregated/:teacherId', authTeacher, checkTeacherParam, async (req, res) => {
    try {
      const appointments = await Appointment.aggregate([
        {
          $match: {
            teacher: ObjectId(req.params.teacherId)
          }
        },
        {
          $group: {
            _id: '$hour',
            days: { $push: '$weekDay'}
          }
        },
        {
          $sort: { _id: 1 }
        }
      ]);

      res.json({ appointments, teacher: req.teacher });
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).send(err);
    }
  });

  /**
   * @api {post} /api/appointment/update/:teacherId Update the hours of all appointments to a new specific hour
   * @apiName UpdateAppointments
   * @apiGroup Appointment
   *
   * @apiParam {number} oldHour   Hour/Time of appointments to be changed from
   * @apiParam {number} newHour   Hour/Time of appointments to be changed to
   */
  router.post('/api/appointment/update/:teacherId', authTeacher, checkTeacherParam, async (req, res) => {
    try {
      if (typeof req.body['oldHour'] !== 'number' || typeof req.body['newHour'] !== 'number') {
        return res.status(400).send('Invalid Parameters.');
      }

      const duplicates = await Appointment
        .find({
          hour: new String(req.body['newHour']),
          teacher: new String(req.params.teacherId)
        })
        .lean();

      if (duplicates.length > 0) {
        return res.status(400).send('Appointments with this hour already exist.');
      }

      const appointments = await Appointment
        .find({
          hour: new String(req.body['oldHour']),
          teacher: new String(req.params.teacherId)
        });

      if (appointments.length === 0) {
        return res.status(400).send('No appointments with this hour found.');
      }

      for (let appoint of appointments) {
        await appoint.updateOne({
          hour: new String(req.body['newHour'])
        });
      }

      io.sockets.emit('appointment', true);

      res.sendStatus(204);
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).send(err);
    }
  });

  /**
   * @api {post} /api/appointment/toggle/:teacherId Toggle an appointment (create or delete it) based on hour and day
   * @apiName ToggleAppointment
   * @apiGroup Appointment
   *
   * @apiParam {number} hour   Hour of appointment
   * @apiParam {number} day    Weekday of appointment
   */
  router.post('/api/appointment/toggle/:teacherId', authTeacher, checkTeacherParam, async (req, res) => {
    try {
      if (typeof req.body['hour'] !== 'number' || typeof req.body['day'] !== 'number') {
        return res.status(400).send('Invalid Parameters.');
      }

      const appointments = await Appointment
        .find({
          hour: new String(req.body['hour']),
          ...(req.body['day'] !== -1 && { weekDay: new String(req.body['day']) }),
          teacher: new String(req.params.teacherId)
        })
        .exec();

      if (appointments.length > 0) {
        // toggle = delete
        for (const appoint of appointments) {
          await appoint.deleteOne();
        }
      } else {
        // toggle = create
        await Appointment.create({
          hour: new String(req.body['hour']),
          weekDay: new String(req.body['day']),
          teacher: new String(req.teacher._id)
        });
      }

      io.sockets.emit('appointment', true);
      res.sendStatus(appointments.length > 0 ? 204 : 201);
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).send(err);
    }
  });


  /**
   * @api {post} /api/appointment Toggle registration to appointment
   * @apiName ToggleAppointmentRegistration
   * @apiGroup appointment
   *
   * @apiParam {String} id Appointment ID
   */
  router.post('/api/appointment/:id/register', async (req, res) => {
    try {
      // gets appointment
      let appointment = await Appointment
        .findById(new String(req.params.id))
        .populate('teacher', '_id timezone local.email notifications.schedule')
        .exec();

      if (!appointment || !appointment.teacher) {
        return res.sendStatus(404);
      }

      // check if another user is registered
      if (appointment.user && appointment.user != req.user._id) {
        return res.status(400).send('another user is registered');
      }

      const settings = await Settings.findOne();

      if (!appointment.user
        && settings.appointmentsRegistrationForerun
        && settings.appointmentsRegistrationForerun > 0) {
        const now = moment.tz(gv(appointment, 'teacher.timezone', ''));

        let nextDate = now.clone().set({
          weekday: appointment.weekDay,
          hour: Math.floor(appointment.hour / 100),
          minute: appointment.hour % 100,
          second: 0
        });

        if (nextDate < now) {
          const tooLateTolerance = gv(settings, 'appointmentsToLateTolerance', 0);
          const minutesSinceAppointment = now.diff(nextDate, 'minutes');
          if (minutesSinceAppointment <= tooLateTolerance) {
            return res
              .status(400)
              .send(`Please wait ${1 + tooLateTolerance - minutesSinceAppointment} more minutes and try again`);
          }

          nextDate = nextDate.add(7, 'days');
        }

        const minutesUntilAppointment = nextDate.diff(now, 'minutes');
        if (minutesUntilAppointment < 60 * settings.appointmentsRegistrationForerun) {
          return res
            .status(400)
            .send(`registrations within ${settings.appointmentsRegistrationForerun} hours before start are not allowed`);
        }
      }

      let cancelledApppointment = null;
      if (!appointment.user) {
        // check if user is already in another appointment and remove it
        cancelledApppointment = await Appointment
          .findOne({
            user: new String(req.user._id)
          })
          .exec();

        if (cancelledApppointment) {
          cancelledApppointment.user = null;
          await cancelledApppointment.save();
        }
      }

      // toggle registration for current user
      const doUnregister = appointment.user && appointment.user == req.user._id;
      appointment.user = doUnregister ? null : req.user;

      await appointment.save();
      // sending broadcast WebSocket for taken/fred appointment
      io.sockets.emit('appointment', appointment);

      res.sendStatus(204);

      // send email notification about change
      const weekdayStr = gv(moment.weekdaysShort(), appointment.weekDay, 'INVALID');
      const hourStr = formatHour(appointment.hour);
      let notification = {
        title: 'Schedule Changed',
        body: `See ${process.env.PUBLIC_URL}/schedule`
      };

      let eventType = APPOINTMENT_EVENTS.register;
      if (doUnregister) {
        eventType = APPOINTMENT_EVENTS.unregister;
        notification.title = `Appointment Canceled: ${weekdayStr}, ${hourStr}`;
      } else if (cancelledApppointment) {
        eventType = APPOINTMENT_EVENTS.reschedule;
        const weekdayOldStr = gv(moment.weekdaysShort(), cancelledApppointment.weekDay, 'INVALID');
        const hourOldStr = formatHour(cancelledApppointment.hour);
        notification.title =
          `Appointment Rescheduled: ${weekdayOldStr}, ${hourOldStr} -> ${weekdayStr}, ${hourStr}`;
      } else {
        notification.title = `Appointment Booked: ${weekdayStr}, ${hourStr}`;
      }

      if (appointment['teacher']['notifications'] && appointment['teacher']['notifications']['schedule']) {
        mail.sendMail(
          appointment['teacher']['local']['email'],
          notification.title,
          notification.body,
          (err) => {
            if (err) {
              // Mail delivery failed
              logger.error(req.url, err);
              logger.error(req.url, 'Failed to send schedule change email.');
              alertDiscord('Failed to send an schedule change email.');
            }
          }
        );
      }

      // keep track of schedule history
      await AppointmentEvent.create({
        user: req.user._id,
        type: eventType
      });
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).send(err);
    }
  });

  /**
   * @api {post} /api/appointment Add new appointment
   * @apiName AddAppointment
   * @apiGroup Appointment
   */
  router.post('/api/appointment/:teacherId', authTeacher, checkTeacherParam, async (req, res) => {
    try {
      // check for duplicates
      const duplicates = await Appointment
        .find({
          weekDay: new String(req.body.weekDay),
          hour: new String(req.body.hour),
          teacher: new String(req.params.teacherId)
        })
        .lean();

      if (duplicates.length > 0) {
        return res.status(400).send('This appointment already exists.');
      }

      await Appointment.create({
        weekDay: new String(req.body.weekDay),
        hour: new String(req.body.hour),
        teacher: new String(req.params.teacherId)
      });

      res.sendStatus(201);
    } catch (err) {
      logger.error(req.url, err);
      res
        .status(err.name === 'ValidationError' ? 400 : 500)
        .send(err);
    }
  });

  /**
   * @api {delete} /api/appointment/remove/:teacherId/:id Deletes any user from appointment
   * @apiName DeleteRegistration
   * @apiGroup Appointment
   */
  router.delete('/api/appointment/remove/:teacherId/:id', authTeacher, checkTeacherParam, async (req, res) => {
    try {
      let appointment = await Appointment
        .findOne({
          _id: new String(req.params.id),
          teacher: new String(req.params.teacherId)
        })
        .exec();

      if (!appointment) {
        return res.sendStatus(404);
      }

      // Remove registration from appointment
      let user = appointment.user;
      appointment.user = null;

      await appointment.save();
      // sending broadcast WebSocket for taken/fred appointment
      io.sockets.emit('appointment', appointment);

      res.sendStatus(204);

      if (user) {
        await AppointmentEvent.create({
          user: user,
          type: APPOINTMENT_EVENTS.remove
        });
      }
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).send(err);
    }
  });
};
