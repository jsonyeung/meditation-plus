import { app } from '../../server.conf.js';
import supertest from 'supertest';
import chai from 'chai';
import { AuthedSupertest } from '../helper/authed-supertest.js';
import Feedback from '../models/feedback.model.js';

const { expect } = chai;

const request = supertest(app);
let user = new AuthedSupertest();
let admin = new AuthedSupertest(
  'Admin User',
  'admin',
  'admin@sirimangalo.org',
  'password',
  'ROLE_ADMIN'
);

describe('Feedback Routes', () => {
  let feedbackId;

  before(async () => {
    await Feedback.deleteMany({});
  });

  after(async () => {
    await Feedback.deleteMany({});
  });

  describe('POST /api/feedback', () => {
    user.authorize();

    it('should respond with 401 when not authenticated', done => {
      request
        .post('/api/feedback')
        .expect(401)
        .end(err => done(err));
    });

    it('should respond with 400 if there is no text', done => {
      user
        .post('/api/feedback')
        .expect(400)
        .end(err => done(err));
    });

    it('should create feedback', done => {
      user
        .post('/api/feedback')
        .send({
          text: 'Nice app!',
          rating: 5,
          device: 'mobile'
        })
        .expect(201)
        .end(async (err) => {
          if (err) return done(err);
          const feedback = await Feedback.findOne({});

          expect(feedback.text).to.equal('Nice app!');
          expect(feedback.rating).to.equal(5);
          expect(feedback.device).to.equal('mobile');
          expect(feedback.user).not.to.exist;
          done();
        });
    });

    it('should create non-anonymous feedback', done => {
      user
        .post('/api/feedback')
        .send({
          text: 'There are many bugs and the support does not respond.',
          rating: 1,
          device: 'desktop',
          anonymous: false
        })
        .expect(201)
        .end(async (err) => {
          if (err) return done(err);
          const feedback = await Feedback.findOne({ user: { $exists: true, $ne: null }});

          feedbackId = feedback._id;

          expect(feedback.text).to.equal('There are many bugs and the support does not respond.');
          expect(feedback.rating).to.equal(1);
          expect(feedback.device).to.equal('desktop');
          expect(feedback.user).to.exist;
          expect(feedback.user.toString()).to.equal(user.user._id.toString());
          done();
        });
    });
  });

  describe('GET /api/feedback', () => {
    user.authorize();
    admin.authorize();

    it('should respond with 401 when not authenticated', done => {
      request
        .get('/api/feedback')
        .expect(401)
        .end(err => done(err));
    });

    it('should respond with 401 when not authenticated as admin', done => {
      user
        .get('/api/feedback')
        .expect(401)
        .end(err => done(err));
    });

    it('should respond with data when authenticated', done => {
      admin
        .get('/api/feedback')
        .expect(200)
        .end((err, res) => {
          if (err) return done(err);
          expect(res.body.length).to.equal(2);
          expect(res.body[1].text).to.equal('Nice app!');
          expect(res.body[1].rating).to.equal(5);
          expect(res.body[1].device).to.equal('mobile');
          expect(res.body[1].user).not.to.exist;
          expect(res.body[0].text).to.equal('There are many bugs and the support does not respond.');
          expect(res.body[0].rating).to.equal(1);
          expect(res.body[0].device).to.equal('desktop');
          expect(res.body[0].user).to.equal(null); // cannot check user object because re-inserted into db between tests
          done(err);
        });
    });
  });

  describe('DELETE /api/feedback', () => {
    user.authorize();

    it('should respond with 401 when not authenticated', done => {
      Feedback.insertMany([
        {
          text: 'Nice app!',
          rating: 5,
          device: 'mobile',
        },
        {
          text: 'There are many bugs and the support does not respond.',
          rating: 1,
          device: 'desktop',
          user: user.user._id,
        }
      ]).then(() => {
        request
          .delete('/api/feedback/' + feedbackId)
          .expect(401)
          .end(err => done(err));
      });
    });

    it('should respond with 401 when authenticated not as admin', done => {
      user
        .delete('/api/feedback/' + feedbackId)
        .expect(401)
        .end(err => done(err));
    });

    it('should respond with 404 when unknown id', done => {
      admin
        .delete('/api/feedback/ABCBBACBBCBA')
        .expect(404)
        .end(err => done(err));
    });

    it('should respond with data when authenticated as admin', done => {
      admin
        .delete('/api/feedback/' + feedbackId)
        .expect(200)
        .end(async (err) => {
          expect(await Feedback.findById(feedbackId)).to.equal(null);
          done(err);
        });
    });
  });
});
