import { app } from '../../server.conf.js';
import moment from 'moment-timezone';
import supertest from 'supertest';
import chai from 'chai';
const { expect } = chai;
import { AuthedSupertest } from '../helper/authed-supertest.js';
import Appointment from '../models/appointment.model.js';
import Settings from '../models/settings.model.js';
import { User } from '../models/user.model.js';
import { USER_ROLES } from '../constants.js';
import timekeeper from 'timekeeper';
import _ from 'lodash-es';
import { parseAppointmentHour } from '../helper/appointment.js';

const request = supertest(app);
let user = new AuthedSupertest();
let user2 = new AuthedSupertest(
  'Second User',
  'user2',
  'user2@sirimangalo.org',
  'password'
);
let admin = new AuthedSupertest(
  'Admin User',
  'admin',
  'admin@sirimangalo.org',
  'password',
  USER_ROLES.ADMIN,
  { timezone: 'Europe/Berlin' }
);

// NOTE: Keep the teacher names in the same lexicographical order
let teacher1 = new AuthedSupertest(
  'Teacher1',
  'teacher1',
  'teacher1@sirimangalo.org',
  'password',
  USER_ROLES.TEACHER,
  { timezone: 'America/Toronto', local: { skype: 'teacher1skype' } }
);
let teacher2 = new AuthedSupertest(
  'Teacher2',
  'teacher2',
  'teacher2@sirimangalo.org',
  'password',
  USER_ROLES.TEACHER,
  { timezone: 'Australia/Sydney' }
);
let teacherAdmin = new AuthedSupertest(
  'Teacher Admin User',
  'teacheradmin',
  'teacheradmin@sirimangalo.org',
  'password',
  USER_ROLES.ADMIN_TEACHER,
  { timezone: 'America/Toronto' }
);

const checkAuthWithTeacherParam = (routePath, method = 'get') => {
  it('should respond with 401 when not authenticated', done => {
    request[method](routePath.replace(':teacherId', teacherAdmin.user._id))
      .expect(401)
      .end(err => done(err));
  });

  it('should respond with 403 for regular user', done => {
    user[method](routePath.replace(':teacherId', teacherAdmin.user._id))
      .expect(401)
      .end(err => done(err));
  });

  it('should respond with 403 for different teacher', done => {
    teacher1[method](routePath.replace(':teacherId', teacherAdmin.user._id))
      .expect(403)
      .end(err => done(err));
  });
};


describe('Appointment Routes', () => {
  let appointment;
  let allAppointments;
  teacherAdmin.authorize();
  teacher1.authorize();
  teacher2.authorize();
  user.authorize();
  user2.authorize();

  before(async () => {
    await User.deleteMany();
    await Appointment.deleteMany();
  });

  beforeEach(async () => {
    await Settings.deleteMany();
    await Appointment.deleteMany();

    allAppointments = await Appointment.insertMany([
      ..._.flatten([0,1,2,3,4,5,6].map(weekDay =>
        [700, 730, 1300, 1330].map(hour => ({ hour, weekDay, teacher: teacherAdmin.user._id }))
      )),
      ..._.flatten([0,1,2,3,4,5,6].map(weekDay =>
        [1001, 1031, 1601, 1631].map(hour => ({ hour, weekDay, teacher: teacher1.user._id }))
      )),
      ..._.flatten([0,1,2,3,4,5,6].map(weekDay =>
        [1502, 502, 302, 2102].map(hour => ({ hour, weekDay, teacher: teacher2.user._id }))
      ))
    ]);

    await Appointment.updateOne({ _id: allAppointments[29]._id }, { $set: { user: user.user._id } });
    await Appointment.updateOne({ _id: allAppointments[30]._id }, { $set: { user: user2.user._id } });

    appointment = await Appointment.findById(allAppointments[29]._id);

    await Settings.create({
      appointmentsTimezone: 'America/Toronto',
      appointmentsToEarlyTolerance: 5,
      appointmentsToLateTolerance: 10
    });
  });

  afterEach(async () => {
    await Appointment.deleteMany();
    await Settings.deleteMany();
  });

  after(async () => await User.deleteMany());

  describe('GET /api/appointment', () => {
    user.authorize();
    teacherAdmin.authorize();
    teacher1.authorize();
    teacher2.authorize();

    it('should respond with 401 when not authenticated', done => {
      request
        .get('/api/appointment')
        .expect(401)
        .end(err => done(err));
    });

    it('should respond with data when authenticated as user', done => {
      user
        .get('/api/appointment')
        .expect(200)
        .end((err, res) => {
          expect(res.body).to.have.property('appointments');
          expect(res.body).to.have.property('ownAppointment');
          expect(res.body).to.have.property('teachers');

          const expectedTeacherPayload = [teacherAdmin, teacher1, teacher2].map(t => ({
            timezone: t.user.timezone,
            name: t.user.name,
            username: t.user.username
          }));

          res.body.teachers.map((item, i) => expect(_.omit(item, ['_id'])).to.eql(expectedTeacherPayload[i]));


          expect(res.body.teacher).to.equal(teacherAdmin._id);
          expect(res.body.appointments.length).to.equal(84);

          res.body.appointments.map(appoint => {
            expect(appoint.weekDay).to.be.a('number');
            expect(appoint.hour).to.be.a('number');
            expect(moment(appoint.nextDate).isValid()).to.equal(true);
            expect([teacherAdmin, teacher1, teacher2].map(t => t.user._id.toString())).to.include(appoint.teacher);
          });

          expect(_.omit(res.body.ownAppointment, ['nextDate', '_id', '__v', 'createdAt', 'updatedAt'])).to.eql({
            hour: 1031,
            weekDay: 0,
            teacher: teacher1.user._id.toString(),
            user: { _id: user.user._id.toString(), name: 'User', username: 'user', hidden: false }
          });

          done(err);
        });
    });

    it('should respond with redacted data when authenticated as another user', done => {
      user2
        .get('/api/appointment')
        .expect(200)
        .end((err, res) => {
          res.body.appointments.map(appoint => {
            expect(appoint.weekDay).to.be.a('number');
            expect(appoint.hour).to.be.a('number');
            expect(moment(appoint.nextDate).isValid()).to.equal(true);
            expect([teacherAdmin, teacher1, teacher2].map(t => t.user._id.toString())).to.include(appoint.teacher);

            if (appoint.user) {
              expect(appoint.user).to.eql({ hidden: true });
            }
          });

          done(err);
        });
    });

    it('should respond with data when authenticated as teacher', done => {
      teacher1
        .get('/api/appointment')
        .expect(200)
        .end((err, res) => {
          res.body.appointments.map(appoint => {
            expect(appoint.weekDay).to.be.a('number');
            expect(appoint.hour).to.be.a('number');
            expect(moment(appoint.nextDate).isValid()).to.equal(true);
            expect([teacherAdmin, teacher1, teacher2].map(t => t.user._id.toString())).to.include(appoint.teacher);

            if (appoint.user) {
              expect(appoint.user.hidden).to.equal(false);
              expect(appoint.user).to.have.property('username');
              expect(appoint.user).to.have.property('name');
              expect(appoint.user).to.have.property('_id');
            }
          });

          done(err);
        });
    });
  });

  describe('GET /api/appointment/aggregated/:teacherId', () => {
    user.authorize();
    teacherAdmin.authorize();
    teacher1.authorize();
    teacher2.authorize();

    checkAuthWithTeacherParam('/api/appointment/aggregated/:teacherId');

    it('should respond with data when authenticated', done => {
      teacher1
        .get(`/api/appointment/aggregated/${teacher1.user._id}`)
        .expect(200)
        .end((err, res) => {
          expect(res.body.appointments.length).to.equal(4);

          expect(_.omit(res.body.teacher, ['_id']), {
            name: teacher1.user.name,
            username: teacher1.user.username,
            timezone: teacher1.user.timezone
          });

          const hours = [1001, 1031, 1601, 1631];
          res.body.appointments.map((group, i) => {
            expect(group['_id']).to.equal(hours[i]);
            expect(group).to.have.property('days');
            expect(group.days).to.eql([0, 1, 2, 3, 4, 5, 6]);
          });

          done(err);
        });
    });

    it('should respond with more data when added', done => {
      Appointment.create({
        weekDay: 2,
        hour: 700,
        teacher: teacher1.user._id
      }).then((res, err) => {
        if (err) return done(err);

        teacher1
          .get(`/api/appointment/aggregated/${teacher1.user._id}`)
          .expect(200)
          .end((err, res) => {
            expect(res.body.appointments.length).to.equal(5);

            expect(_.omit(res.body.teacher, ['_id']), {
              name: teacher1.user.name,
              username: teacher1.user.username,
              timezone: teacher1.user.timezone
            });

            expect(res.body.appointments[0]['days']).to.eql([2]);
            done(err);
          });
      });
    });
  });

  describe('POST /api/appointment/:id/register', () => {
    user.authorize();
    user2.authorize();
    teacherAdmin.authorize();
    teacher1.authorize();
    teacher2.authorize();

    it('should respond with 401 when not authenticated', done => {
      request
        .post(`/api/appointment/${appointment._id}/register`)
        .expect(401)
        .end(err => done(err));
    });

    it('should respond with 204 on success', done => {
      user2
        .post(`/api/appointment/${allAppointments[50]._id}/register`)
        .expect(204)
        .end(err => {
          if (err) return done(err);

          Appointment.find({ user: user2.user._id}, (err, res) => {
            if (err) return done(err);
            expect(res.length).to.equal(1);
            expect(res[0]._id).to.eql(allAppointments[50]._id);

            // deregistration should work, too
            user2
              .post(`/api/appointment/${allAppointments[50]._id}/register`)
              .expect(204)
              .end(err => {
                if (err) return done(err);

                Appointment.find({ user: user2.user._id}, (err, res) => {
                  if (err) return done(err);
                  expect(res.length).to.equal(0);
                  done();
                });
              });
          });
        });
    });

    it('should respond with 400 when already taken', done => {
      user2
        .post(`/api/appointment/${appointment._id}/register`)
        .expect(400)
        .end(err => done(err));
    });
  });

  describe('POST /api/appointment/:teacherId', () => {
    user.authorize();
    admin.authorize();
    teacherAdmin.authorize();
    teacher1.authorize();
    teacher2.authorize();

    checkAuthWithTeacherParam('/api/appointment/:teacherId', 'post');

    it('should return 201 when authenticated as teacher admin', done => {
      teacherAdmin
        .post(`/api/appointment/${teacherAdmin.user._id}`)
        .send({
          hour: 0,
          weekDay: 0
        })
        .expect(201)
        .end(err => done(err));
    });

    it('should return 201 when authenticated as teacher admin for different teacher', done => {
      teacherAdmin
        .post(`/api/appointment/${teacher2.user._id}`)
        .send({
          hour: 0,
          weekDay: 0
        })
        .expect(201)
        .end(err => done(err));
    });

    it('should return 201 when authenticated as teacher', done => {
      teacher1
        .post(`/api/appointment/${teacher1.user._id}`)
        .send({
          hour: 0,
          weekDay: 0
        })
        .expect(201)
        .end(err => done(err));
    });

    it('should return with 400 if appointment already exists', done => {
      teacher1
        .post(`/api/appointment/${teacher1.user._id}`)
        .send({
          hour: 0,
          weekDay: 0
        })
        .expect(201)
        .end(err => done(err));
    });
  });

  describe('POST /api/appointment/toggle', () => {
    user.authorize();
    admin.authorize();
    teacherAdmin.authorize();
    teacher1.authorize();
    teacher2.authorize();

    checkAuthWithTeacherParam('/api/appointment/toggle/:teacherId', 'post');

    it('should respond with 201 when trying to add a new appointment', done => {
      teacher1
        .post(`/api/appointment/toggle/${teacher1.user._id}`)
        .send({
          hour: 850,
          day: 3
        })
        .expect(201)
        .end(err => {
          if (err) return done(err);

          Appointment
            .find({ teacher: teacher1.user._id, hour: 850, weekDay: 3 }, (err, res) => {
              expect(res.length).to.equal(1);
              done(err);
            });
        });
    });

    it('should respond with 204 when trying to delete an existing appointment', done => {
      teacher1
        .post(`/api/appointment/toggle/${teacher1.user._id}`)
        .send({
          hour: 1001,
          day: 1
        })
        .expect(204)
        .end(err => {
          if (err) return done(err);

          Appointment
            .find({ teacher: teacher1.user._id, hour: 1001, weekDay: 1 }, (err, res) => {
              expect(res.length).to.equal(0);
              done(err);
            });
        });
    });

    it('should respond with 201 when trying to add a new appointment as teacher admin', done => {
      teacherAdmin
        .post(`/api/appointment/toggle/${teacher1.user._id}`)
        .send({
          hour: 123,
          day: 1
        })
        .expect(201)
        .end(err => {
          if (err) return done(err);

          Appointment
            .find({ teacher: teacher1.user._id, hour: 123, weekDay: 1 }, (err, res) => {
              expect(res.length).to.equal(1);
              done(err);
            });
        });
    });

    it('should respond with 204 when trying to delete an existing appointment as teacher admin', done => {
      teacherAdmin
        .post(`/api/appointment/toggle/${teacher1.user._id}`)
        .send({
          hour: 1001,
          day: 1
        })
        .expect(204)
        .end(err => {
          if (err) return done(err);

          Appointment
            .find({ teacher: teacher1.user._id, hour: 1001, weekDay: 1 }, (err, res) => {
              expect(res.length).to.equal(0);
              done(err);
            });
        });
    });
  });

  describe('POST /api/appointment/update/:teacherId', () => {
    user.authorize();
    admin.authorize();
    teacherAdmin.authorize();
    teacher1.authorize();
    teacher2.authorize();

    checkAuthWithTeacherParam('/api/appointment/update/:teacherId', 'post');

    it('should respond with 400 when given invalid params', done => {
      teacher1
        .post(`/api/appointment/update/${teacher1.user._id}`)
        .send({
          oldHour: 'invalid'
        })
        .expect(400)
        .end(err => done(err));
    });

    it('should respond with 400 when appointment already exists', done => {
      teacher1
        .post(`/api/appointment/update/${teacher1.user._id}`)
        .send({
          oldHour: 1300,
          newHour: 1300
        })
        .expect(400)
        .end(err => done(err));
    });

    it('should respond with 400 when no appointment with given hour can be found', done => {
      teacher1
        .post(`/api/appointment/update/${teacher1.user._id}`)
        .send({
          oldHour: 500,
          newHour: 600
        })
        .expect(400)
        .end(err => done(err));
    });

    it('should respond with 204 when params are valid', done => {
      teacher1
        .post(`/api/appointment/update/${teacher1.user._id}`)
        .send({
          oldHour: 1001,
          newHour: 300
        })
        .expect(204)
        .end(err => {
          if (err) return done(err);

          Appointment
            .find({ hour: 300 }, (err, res) => {
              expect(res.length).to.equal(7);

              done(err);
            });
        });
    });
  });

  describe('DELETE /api/appointment/remove/:teacherId/:id', () => {
    user.authorize();
    user2.authorize();
    admin.authorize();
    teacherAdmin.authorize();
    teacher1.authorize();
    teacher2.authorize();

    checkAuthWithTeacherParam('/api/appointment/remove/:teacherId/someid', 'delete');

    it('should correctly delete when authenticated', done => {
      teacher1
        .delete(`/api/appointment/remove/${teacher1.user._id}/${allAppointments[29]._id}`)
        .expect(204)
        .end(err => {
          if (err) return done(err);

          // check if really deleted
          Appointment
            .findById(allAppointments[29]._id, (err, res) => {
              expect(res.user).to.equal(null);
              done(err);
            });
        });
    });

    it('should correctly delete when authenticated as teacher admin', done => {
      teacherAdmin
        .delete(`/api/appointment/remove/${teacher1.user._id}/${allAppointments[30]._id}`)
        .expect(204)
        .end(err => {
          if (err) return done(err);

          // check if really deleted
          Appointment
            .findById(allAppointments[30]._id, (err, res) => {
              expect(res.user).to.equal(null);
              done(err);
            });
        });
    });
  });

  describe('GET /api/appointment/meeting', () => {
    user2.authorize();
    user.authorize();
    teacherAdmin.authorize();
    teacher1.authorize();
    teacher2.authorize();

    it('should respond with 401 when not authenticated', done => {
      request
        .get('/api/appointment/meeting')
        .expect(401)
        .end(err => done(err));
    });

    it('should respond with 200 and isActive = false if schedule is disabled', async () => {
      await Settings.updateOne({}, { disableAppointments: true });
      const res = await user2.get('/api/appointment/meeting');
      expect(res.status).to.equal(200);
      expect(res.body.isActive).to.equal(false);
      expect(res.body).to.not.have.property('callInformation');
      await Settings.updateOne({}, { disableAppointments: false });
    });

    it('should respond with 200 and isActive = false if user has no appointment', async () => {
      await appointment.updateOne({ user: null });
      timekeeper.travel(moment.tz(teacher1.user.timezone)
        .weekday(appointment.weekDay)
        .hour(parseAppointmentHour(appointment.hour).hour)
        .minute(parseAppointmentHour(appointment.hour).minute)
        .second(0)
        .toDate()
      );
      const res = await user.get('/api/appointment/meeting');
      expect(res.status).to.equal(200);
      expect(res.body.isActive).to.equal(false);
      expect(res.body).to.not.have.property('callInformation');
    });

    it('should respond with 200 and isActive = false if user requests at wrong time', async () => {
      let res;

      timekeeper.travel(moment.tz(teacher1.user.timezone)
        .weekday(appointment.weekDay)
        .hour(parseAppointmentHour(appointment.hour).hour + 1)
        .minute(parseAppointmentHour(appointment.hour).minute)
        .second(0)
        .toDate()
      );

      res = await user.get('/api/appointment/meeting');
      expect(res.status).to.equal(200);
      expect(res.body.isActive).to.equal(false);
      expect(res.body).to.not.have.property('callInformation');

      timekeeper.travel(moment.tz(teacher1.user.timezone)
        .weekday(appointment.weekDay + 2)
        .hour(parseAppointmentHour(appointment.hour).hour + 1)
        .minute(parseAppointmentHour(appointment.hour).minute)
        .second(0)
        .toDate()
      );

      res = await user.get('/api/appointment/meeting');
      expect(res.status).to.equal(200);
      expect(res.body.isActive).to.equal(false);
      expect(res.body).to.not.have.property('callInformation');

      timekeeper.travel(moment.tz(teacher1.user.timezone)
        .weekday(appointment.weekDay + 2)
        .hour(parseAppointmentHour(appointment.hour).hour)
        .minute(parseAppointmentHour(appointment.hour).minute - 6)
        .second(0)
        .toDate()
      );

      res = await user.get('/api/appointment/meeting');
      expect(res.status).to.equal(200);
      expect(res.body.isActive).to.equal(false);
      expect(res.body).to.not.have.property('callInformation');


      timekeeper.travel(moment.tz(teacher1.user.timezone)
        .weekday(appointment.weekDay)
        .hour(parseAppointmentHour(appointment.hour).hour)
        .minute(parseAppointmentHour(appointment.hour).minute + 11)
        .second(0)
        .toDate()
      );

      res = await user.get('/api/appointment/meeting');
      expect(res.status).to.equal(200);
      expect(res.body.isActive).to.equal(false);
      expect(res.body).to.not.have.property('callInformation');
    });

    it('should respond with 200 and isActive = true and call info if user requests at right time', async () => {
      timekeeper.travel(moment.tz(teacher1.user.timezone)
        .weekday(appointment.weekDay)
        .hour(parseAppointmentHour(appointment.hour).hour)
        .minute(parseAppointmentHour(appointment.hour).minute - 5)
        .second(0)
        .toDate()
      );

      const res1 = await user.get('/api/appointment/meeting');
      expect(res1.status).to.equal(200);
      expect(res1.body.isActive).to.equal(true);
      expect(res1.body.callInformation).to.equal('teacher1skype');

      timekeeper.travel(moment.tz(teacher1.user.timezone)
        .weekday(appointment.weekDay)
        .hour(parseAppointmentHour(appointment.hour).hour)
        .minute(parseAppointmentHour(appointment.hour).minute + 9)
        .second(50)
        .toDate()
      );

      const res2 = await user.get('/api/appointment/meeting');
      expect(res2.status).to.equal(200);
      expect(res2.body.isActive).to.equal(true);
      expect(res1.body.callInformation).to.equal('teacher1skype');

      timekeeper.travel(moment.tz(teacher1.user.timezone)
        .weekday(appointment.weekDay)
        .hour(parseAppointmentHour(appointment.hour).hour)
        .minute(parseAppointmentHour(appointment.hour).minute)
        .second(0)
        .toDate()
      );
      const res3 = await user.get('/api/appointment/meeting');
      expect(res3.status).to.equal(200);
      expect(res3.body.isActive).to.equal(true);
      expect(res1.body.callInformation).to.equal('teacher1skype');
    });
  });
});
