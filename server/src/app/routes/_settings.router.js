import { isBoolean } from 'lodash-es';
import moment from 'moment-timezone';
import Settings from '../models/settings.model.js';
import { logger } from '../helper/logger.js';
import { authAdmin } from '../helper/auth.js';
import { cleanBody, cleanParams } from '../helper/clean-body.js';

export default (app, router) => {
  /**
   * @api {get} /api/settings Get the settings entity
   * @apiName GetSettings
   * @apiGroup Settings
   */
  router.get('/api/settings', async (req, res) => {
    try {
      // Find settings entity or create a new one with the in the
      // Settings model defined defaults.
      const settings = await Settings.findOneAndUpdate({}, {}, {
        new: true,
        upsert: true,
        setDefaultsOnInsert: true
      });

      res.json(settings);
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).send(err);
    }
  });

  /**
   * @api {put} /api/settings Change the settings entity
   * @apiName SetProperty
   * @apiGroup Settings
   */
  router.put('/api/settings/:property', cleanBody, cleanParams, authAdmin, async (req, res) => {
    try {
      if (!req.params.property || typeof(req.body.value) === 'undefined') {
        return res.sendStatus(400);
      }

      const settings = {};
      settings[req.params.property] = req.body.value;

      // Find settings entity or create a new one with the in the
      // Settings model defined defaults and then update it.
      await Settings.findOneAndUpdate({}, settings, {
        new: true,
        upsert: true,
        setDefaultsOnInsert: true
      });

      res.sendStatus(204);
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).send(err);
    }
  });

  /**
   * @api {post} /api/settings/disable-schedule   Disable (or enable) the appointments schedule for a certain time range.
   * @apiName DisableSchedule
   * @apiGroup Settings
   *
   * @apiParam {boolean}  disabled    Whether or not the schedule should be disabled
   * @apiParam {string}   dateFrom    The start date of the "inactive" time range in the format YYYY-MM-DD
   * @apiParam {string}   dateTo      The end date of the "inactive" time range in the format YYYY-MM-DD
   */
  router.post('/api/settings/disable-schedule', authAdmin, async (req, res) => {
    try {
      const disabled = req.body.disabled;

      if (!isBoolean(disabled)) {
        return res.status(400).json({ error: 'Property "disabled" must be boolean.' });
      }

      const settings = await Settings.findOne({});

      if (disabled === false) {
        await Settings.updateOne({}, {
          disableAppointments: false,
          disableAppointmentsFrom: undefined,
          disableAppointmentsUntil: undefined
        });
        return res.status(204).json();
      }

      if (!req.body.dateFrom && !req.body.dateTo) {
        await Settings.updateOne({}, { disableAppointments: true });
        return res.status(204).json();
      }

      const dateFrom = moment.tz(req.body.dateFrom, 'YYYY-MM-DD', true, settings.disableAppointmentsTimezone);
      const dateTo = moment.tz(req.body.dateTo, 'YYYY-MM-DD', true, settings.disableAppointmentsTimezone);

      if (!dateFrom.isValid()) {
        return res.status(400).json({ error: 'Invalid start date (must be in format YYYY-MM-DD).' });
      }

      if (!dateTo.isValid()) {
        return res.status(400).json({ error: 'Invalid end date (must be in format YYYY-MM-DD).' });
      }

      const now = moment.tz(settings.disableAppointmentsTimezone);

      if (dateTo.isBefore(now, 'day')) {
        return res.status(400).json({ error: 'Invalid end date (in the past).' });
      }

      if (dateTo.isBefore(dateFrom, 'day')) {
        return res.status(400).json({ error: 'Invalid end date (must be after start date).' });
      }

      await await Settings.updateOne({}, {
        disableAppointments: true,
        disableAppointmentsFrom: dateFrom.format('YYYY-MM-DD'),
        disableAppointmentsUntil: dateTo.format('YYYY-MM-DD')
      });

      return res.status(204).json();
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).send(err);
    }
  });
};
