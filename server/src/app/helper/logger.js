import winston from 'winston';
import request from 'superagent';
const { combine, timestamp, colorize, cli, printf } = winston.format;

const cliFormat = printf(({ level, message, timestamp }) => {
  return `${timestamp} ${level}: ${message}`;
});

const logger = winston.createLogger({
  format: combine(
    timestamp(),
    cli(),
    cliFormat,
    colorize()
  ),
  transports: [
    new winston.transports.Console({
      level: 'debug',
      handleExceptions: true,
      colorize: true
    }),
  ]
});

const alertDiscord = async (message) => {
  if (process.env.NODE_ENV !== 'test' && process.env.DISCORD_ALERT_WEBHOOK) {
    try {
      await request
        .post(process.env.DISCORD_ALERT_WEBHOOK)
        .retry(3)
        .set('Content-Type', 'application/json')
        .send({
          username: 'Meditation+ Alert Bot',
          content: `:warning: [${process.env.PUBLIC_URL}]: ${message}`
        });
    } catch (err) {
      logger.error(err);
    }
  }
};

export { logger, alertDiscord };
