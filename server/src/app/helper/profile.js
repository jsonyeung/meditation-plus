import Meditation from '../models/meditation.model.js';
import moment from 'moment-timezone';
import timezone from './timezone.js';

import mongoose from 'mongoose';
let ObjectId = mongoose.Types.ObjectId;

export class ProfileHelper {
  constructor(user = null) {
    this.user = user;
    this.utcOffset = 0;
    if (user) {
      this.utcOffset = timezone(this.user, 0).utcOffset();
    }
  }

  /**
   * Calculate general, non time-specific statistic
   * for a certain user.
   *
   * @param  {User}   user   Valid user
   * @return {Object}        General profile stats
   */
  async getGeneralStats() {
    const defaultValues = {
      _id: null,
      walking: 0,
      sitting: 0,
      total: 0,
      avgSessionTime: 0,
      countOfSessions: 0
    };

    const data = await Meditation.aggregate([
      { $match: { user: ObjectId(this.user._id) } },
      {
        $group: {
          _id: null,
          walking: { $sum: '$walking' },
          sitting: { $sum: '$sitting' },
          total: { $sum: { $add: ['$walking', '$sitting'] } },
          avgSessionTime: { $avg: { $add: ['$walking', '$sitting'] } },
          countOfSessions: { $sum: 1 }
        }
      }
    ]);

    return data && data.length > 0 ? data[0] : defaultValues;
  }

  /**
   * Calculate profile data for the current week (since last monday).
   *
   * @param  {User}   user   Valid user
   * @return {Object}        Profile stats for this week
   */
  async getWeekChartData() {
    return Meditation.aggregate([
      {
        $match: {
          user: ObjectId(this.user._id),
          createdAt: {
            $gte: timezone(this.user, moment.utc()).startOf('isoweek').toDate()
          }
        }
      },
      {
        $group: {
          _id: { $dayOfWeek: { $add: ['$createdAt', this.utcOffset * 60000] } },
          walking: { $sum: '$walking' },
          sitting: { $sum: '$sitting' }
        }
      }
    ]);
  }

  /**
   * Calculate profile data for the current month (since first day in month).
   *
   * @param  {User}   user   Valid user
   * @return {Object}        Profile stats for this month
   */
  async getMonthChartData(monthOffset = 0) {
    const monthStart = timezone(this.user, moment()).startOf('month').subtract(monthOffset, 'months');

    return Meditation.aggregate([
      {
        $match: {
          user: ObjectId(this.user._id),
          createdAt: {
            $gte: monthStart.toDate(),
            $lte: monthStart.endOf('month').toDate()
          }
        }
      },
      {
        $group: {
          _id: { $dayOfMonth: { $add: ['$createdAt', this.utcOffset * 60000] } },
          walking: { $sum: '$walking' },
          sitting: { $sum: '$sitting' }
        }
      }
    ]);
  }

  /**
   * Calculate profile data for the past year.
   *
   * @param  {User}   user   Valid user
   * @return {Object}        Profile stats for past year
   */
  async getYearChartData() {
    return Meditation.aggregate([
      {
        $match: {
          user: ObjectId(this.user._id),
          createdAt: {
            $gte: timezone(this.user, moment()).subtract(1, 'year').add(1, 'month').startOf('month').toDate()
          }
        }
      },
      {
        $group: {
          _id: { $month: { $add: ['$createdAt', this.utcOffset * 60000] } },
          walking: { $sum: '$walking' },
          sitting: { $sum: '$sitting' }
        }
      }
    ]);
  }

  /**
   * Helper function for returning an object containing data from
   * all three methods (week, month, year).
   *
   * @param  {User}   user   Valid user
   * @return {Object}        Chart data
   */
  async getChartData() {
    return {
      week: await this.getWeekChartData(),
      month: await this.getMonthChartData(),
      year: await this.getYearChartData()
    };
  }

  /**
   * Get number of total and current consecutive days of meditation.
   *
   * @param  {User}   user Valid user
   * @return {Object}      Object containing both values
   */
  async getConsecutiveDays() {
    const daysMeditated = await Meditation.aggregate([
      { $match: { user: ObjectId(this.user._id) } },
      {
        $group: {
          _id: {
            $let: {
              vars: {
                createdAtTz: { $add: ['$createdAt', this.utcOffset * 60000] }
              },
              in: {
                year: { $year: '$$createdAtTz' },
                month: { $month: '$$createdAtTz' },
                day: { $dayOfMonth: '$$createdAtTz' }
              }
            }
          }
        }
      },
      {
        $sort: {
          '_id.year': -1,
          '_id.month': -1,
          '_id.day': -1
        }
      },
    ]);

    const result = {
      current: 0,
      total: 0,
      max: 0,
      badges: 0
    };

    // no consecutive days
    if (daysMeditated.length < 2) return result;

    // necessary helper function to deal with aggregation
    // data structure
    const toMoment = obj => moment
      .utc()
      .year(obj._id.year)
      .month(obj._id.month - 1 < 0 ? 11 : obj._id.month - 1)
      .date(obj._id.day);

    // Allow any meditation between now and start of last day
    // to indicate the 'current' range
    let rangeUntilToday = moment.utc().startOf('day').subtract(1, 'day') < toMoment(daysMeditated[0]);
    let rangeCounter = 0;

    const updateValues = () => {
      if (rangeCounter <= 0) {
        return;
      }

      // to not miss the starting day ...
      rangeCounter++;

      // update values
      result.max = Math.max(result.max, rangeCounter);
      result.total += rangeCounter;

      if (rangeUntilToday) {
        result.current = rangeCounter;
      }

      // update badges
      if (rangeCounter >= 10) {
        result.badges += Math.floor(rangeCounter / 10);
      }

      // reset counter
      rangeCounter = 0;
    };

    let dayBefore = toMoment(daysMeditated[0]);
    for (let i = 1; i < daysMeditated.length; i++) {
      let currentDay = toMoment(daysMeditated[i]);

      if (Math.abs(dayBefore.diff(currentDay, 'days')) <= 1) {
        rangeCounter++;
      } else {
        updateValues();
        rangeUntilToday = false;
      }

      dayBefore = currentDay;
    }
    updateValues();

    return result;
  }

  /**
   * Calculate commitment stats
   * @param  {Commitment} commitment  A commitment
   * @return {Number}                 Progress in percentage
   */
  async getCommitmentStatus(commitment = null) {
    const aggDaysAgo = commitment.type === 'daily' ? 10 : 7;
    const data = await Meditation.aggregate([
      {
        $match: {
          user: ObjectId(this.user._id),
          createdAt: {
            $gte: timezone(this.user, moment()).subtract(aggDaysAgo, 'days').toDate()
          }
        }
      },
      {
        $group: {
          _id: { $dayOfMonth: { $add: ['$createdAt', this.utcOffset * 60000] } },
          total: { $sum: { $add: ['$walking', '$sitting'] } }
        }
      }
    ]);

    if (!data || !data.length) {
      return 0;
    }

    const reachedMaxValue = commitment.type === 'daily'
      ? commitment.minutes * aggDaysAgo
      : commitment.minutes;

    let reachedValue = 0;

    // sum up reached minutes until their max value
    data.map(doc => reachedValue = Math.min(
      commitment.type === 'daily'
        ? reachedValue + Math.min(doc.total, commitment.minutes)
        : reachedValue + doc.total,
      reachedMaxValue
    ));

    return Math.round(100 * (reachedValue / reachedMaxValue));
  }
}
