import mpath from 'mpath';
import { set, cloneDeep } from 'lodash-es';

/**
 * Validate an object using a mongoose model.
 *  - Ignore any additional keys.
 *  - Values are automatically casted by mongoose
 *  - id, version and timestamp keys are ignored
 *
 * @param {Object} patch
 * @param {Model} model
 */
export const validateSchemaPartial = async (
  patch,
  model
) => {
  const schemaPaths = Object.keys(model.schema.paths);

  const patchPaths = [];
  let cleanPatch = {};

  const ignoredPaths = ['_id', '__v', 'createdAt', 'updatedAt'];
  for (const path of schemaPaths) {
    if (ignoredPaths.indexOf(path) !== -1) {
      continue;
    }

    const value = mpath.get(path, patch);
    if (typeof value !== 'undefined') {
      patchPaths.push(path);
      set(cleanPatch, path, value); // mpath.set() does not create a nested structure
    }
  }

  if (patchPaths.length === 0) {
    return {};
  }

  // prevent changes to the original object done by model.validate()
  cleanPatch = cloneDeep(cleanPatch);
  await model.validate(cleanPatch, patchPaths);

  return cleanPatch;
};
