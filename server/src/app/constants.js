export const USER_ROLES = {
  ADMIN_TEACHER: 'ROLE_ADMIN_TEACHER',
  ADMIN: 'ROLE_ADMIN',
  TEACHER: 'ROLE_TEACHER',
  USER: 'ROLE_USER'
};

export const QUERY_TEACHER = {
  $or: [
    { role: USER_ROLES.ADMIN_TEACHER },
    { role: USER_ROLES.TEACHER }
  ]
};

export const QUERY_ADMIN = {
  $or: [
    { role: USER_ROLES.ADMIN_TEACHER },
    { role: USER_ROLES.ADMIN }
  ]
};

export const COMMITMENT_TYPES = ['daily', 'weekly'];
