import authRoutes from './routes/_authentication.router.js';
import messageRoutes from './routes/_message.router.js';
import questionRoutes from './routes/_question.router.js';
import testimonialRoutes from './routes/_testimonial.router.js';
import commitmentRoutes from './routes/_commitment.router.js';
import meditationRoutes from './routes/_meditation.router.js';
import videoSuggestionRoutes from './routes/_video-suggestion.router.js';
import profileRoutes from './routes/_profile.router.js';
import broadcastRoutes from './routes/_broadcast.router.js';
import appointmentRoutes from './routes/_appointment.router.js';
import userRoutes from './routes/_user.router.js';
import liveRoutes from './routes/_livestream.router.js';
import settingsRoutes from './routes/_settings.router.js';
import analyticsRoutes from './routes/_analytics.router.js';
import pushRoutes from './routes/_push.router.js';
import feedbackRoutes from './routes/_feedback.router.js';
import mongoose from 'mongoose';
import requestMiddleware from './requests.middleware.js';

export default (app, router, passport, io) => {

  // Express Middlware to use for all requests
  router.use(requestMiddleware);
  authRoutes(app, router, passport);
  testimonialRoutes(app, router, io);
  messageRoutes(app, router, io);
  questionRoutes(app, router, io);
  videoSuggestionRoutes(app, router, io);
  broadcastRoutes(app, router);
  commitmentRoutes(app, router);
  meditationRoutes(app, router, io);
  profileRoutes(app, router);
  appointmentRoutes(app, router, io);
  userRoutes(app, router, io);
  liveRoutes(app, router);
  settingsRoutes(app, router);
  analyticsRoutes(app, router);
  feedbackRoutes(app, router);
  pushRoutes(app, router);

  // Provide a simple status page
  // Return 204 (No Content) when mongoose is connected
  // Otherwise 503 (Service Unavailable)
  router.get('/status', (req, res) => {
    return res.sendStatus(mongoose.connection.readyState ? 204 : 503);
  });
};
