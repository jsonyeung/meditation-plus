import mongoose from 'mongoose';
import bcrypt from 'bcryptjs';
import CommitmentUser from './commitment-user.model.js';
import Message from './message.model.js';
import Appointment from './appointment.model.js';
import Meditation from './meditation.model.js';
import Question from './question.model.js';
import Testimonial from './testimonial.model.js';
import { USER_ROLES } from '../constants.js';

let userSchema = mongoose.Schema({
  username: {
    type: String,
    unique: true,
    index: true,
    sparse: true, // allow null
    validate: /^[a-zA-Z][a-zA-Z0-9-_.]{3,20}$/
  },
  local : {
    password : String,
    email : { type : String, unique : true },
    skype: String
  },
  avatarImageToken: String,
  suspendedUntil: Date,
  name: { type: String, maxlength: 30 },
  role : {
    type : String,
    enum: [
      USER_ROLES.ADMIN_TEACHER,
      USER_ROLES.ADMIN,
      USER_ROLES.TEACHER,
      USER_ROLES.USER
    ]
  },
  showEmail: Boolean,
  hideStats: Boolean,
  description: { type: String, maxlength: 300 },
  website: { type: String, maxlength: 100 },
  country: String,
  lastActive: Date,
  lastMeditation: { type: Date, required: true, default: new Date(0) },
  lastLike: Date,
  sound: { type: String, default: '/assets/audio/bell1.mp3' },
  stableBell: { type: Boolean, default: false },
  timezone: String,
  verified: { type: Boolean, default: false },
  acceptedGdpr: { type: Boolean, default: false },
  acceptedGdprDate: Date,
  verifyToken: String,
  recoverUntil: Date,
  prefer12HourFormat: Boolean,
  notifications: {
    schedule: Boolean,
    livestream: Boolean,
    message: { type: Boolean, default: true },
    meditation: Boolean,
    question: { type: Boolean, default: true },
    // relevant for admins only
    testimonial: Boolean
  }
}, {
  timestamps: true
});

userSchema.methods.generateHash = function(password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8));
};

userSchema.methods.validPassword = function(password) {
  return bcrypt.compareSync(password, this.local.password);
};

userSchema.pre('remove', async function(next) {
  await Message.deleteMany({ user: this._id }).exec();
  await Appointment.findOneAndUpdate({ user: this._id }, { user: null }).exec();
  await Meditation.deleteMany({ user: this._id }).exec();
  await Question.deleteMany({ user: this._id }).exec();
  await Testimonial.deleteMany({ user: this._id }).exec();
  await CommitmentUser.deleteMany({ user: this._id }).exec();
  next();
});

const User = mongoose.model('User', userSchema);

const writableFields = [
  'username', 'name', 'hideStats',
  'description', 'website', 'country',
  'sound', 'stableBell', 'timezone',
  'prefer12HourFormat', 'notifications',
  'showEmail'
];

const visiblePublicFields = [
  '_id', 'createdAt', 'avatarImageToken', ...writableFields
];

export { User, writableFields, visiblePublicFields };
