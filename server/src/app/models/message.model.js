import mongoose from 'mongoose';

let messageSchema = mongoose.Schema({
  text : { type: String, required: true, maxlength: 1000 },
  user : { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
  edited: Boolean,
  deleted: Boolean,
  private: Boolean // deprecated, but should be kept for backwards compatibility
}, {
  timestamps: true
});

export default mongoose.model('Message', messageSchema);
