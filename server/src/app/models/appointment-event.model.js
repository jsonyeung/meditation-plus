import mongoose from 'mongoose';

const APPOINTMENT_EVENTS = {
  register: 'register',
  reschedule: 'reschedule',
  unregister: 'unregister',
  remove: 'remove' // by admin
};

const appointmentEventSchema = mongoose.Schema({
  user: {
    required: true,
    type : mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  type: {
    required: true,
    type: String,
    enum: Object.values(APPOINTMENT_EVENTS)
  }
}, {
  timestamps: true
});

const AppointmentEvent = mongoose.model('AppointmentEvent', appointmentEventSchema);

export { AppointmentEvent, APPOINTMENT_EVENTS };
