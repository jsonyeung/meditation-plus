import mongoose from 'mongoose';

let feedbackSchema = mongoose.Schema({
  text: { type: String, required: true, maxlength: 1000, index: 'text' },
  user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  rating: { type: Number, min: 1, max: 5 },
  device: { type: String, enum: ['desktop', 'tablet', 'mobile'] }
}, {
  timestamps: true
});

export default mongoose.model('Feedback', feedbackSchema);
